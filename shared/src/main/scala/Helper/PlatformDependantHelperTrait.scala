package Helper

import org.widok.Channel

import scala.concurrent.Future

/**
  * Created by nephtys on 8/28/16.
  */
abstract trait PlatformDependantHelperTrait {

  def debounce[T](x: Channel[T])(ms: Long): Channel[T]
}
