package Helper

import org.widok.{BufSet, Buffer, Channel, DeltaDict, Dict, ReadBufSet, ReadBuffer}



import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global

import com.softwaremill.macwire._

/**
  * Created by nephtys on 8/26/16.
  */
object WidokVarHelpers {

  import Helper._

  implicit class SomeChannel[T](x : Channel[T]) {
    //private val code = wire[Helper.PlatformDependantHelperTrait]
    //def debounce(ms : Long) : Channel[T] = {
     // ??? //code.debounce(x)(ms)
    //}
    def foreach(i : T => Unit) : Unit = x.attach(i)
    def backgroundforeach(i : T => Unit) : Unit = x.attach(v => Future{i.apply(v)})
  }


  implicit class MapBufferChannel[K,V](x : DeltaDict[K,V]) {
    def keySeq : ReadBuffer[K] = {
      val y = Buffer[K]()
      x.keys.changes.attach(f => f match {
        case m : BufSet.Delta.Insert[K] => y.+=(m.value)
        case m : BufSet.Delta.Remove[K] => y.-=(m.value)
        case m : BufSet.Delta.Clear[K] => y.clear()
      }
      )
      y
    }
    def pairSeq : ReadBuffer[(K, V)] = {
      val y = Buffer[(K, V)]()
      x.changes.attach(f => f match {
        case m : Dict.Delta.Insert[K,V] => y.+=((m.key, m.value ))
        case m : Dict.Delta.Update[K,V] => y.update(f => {
          if (f._1 == m.key) {
            (m.key, m.value)
          } else {
            f
          }
        })
        case m : Dict.Delta.Remove[K,V] => y.find$(element => element._1 == m.key).foreach(element => y.-=(element))
        case m : Dict.Delta.Clear[K,V] => y.clear()
      }
      )
      y
    }
  }

  implicit class ImprovedBuffer[V](x : ReadBuffer[V]) {
    def toBufSet : ReadBufSet[V] = {
        val y = BufSet[V]()
      x.changes.attach(f => f match {
        case m : Buffer.Delta.Insert[V] => y.+=(m.element)
        case m : Buffer.Delta.Replace[V] => {
          y.-=(m.reference)
          y.+=(m.element)
        }
        case m : Buffer.Delta.Remove[V] => y.-=(m.element)
        case m : Buffer.Delta.Clear[V] => y.clear()
      })
      y
    }
  }



}
