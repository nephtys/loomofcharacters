package Helper

/**
  * Created by Christopher on 20.07.2016.
  */
class Memorization[K, V] {
      var lastKey : Option[K] = None
    var lastValue : Option[V] = None
  def memorize(key : K)(x : => V) : V = {
    if (lastKey.contains(key) && lastValue.isDefined) {
      lastValue.get
    } else {
      //recalc
      val v : V = x
        lastKey = Some(key)
      lastValue = Some(v)
      v
    }
  }
}
