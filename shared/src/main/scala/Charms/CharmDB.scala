package Charms

import AtomExal.{AtomicExchange, Charmable, PurchaseCharm}
import org.widok.{Channel, Dict}
import rx.Ctx

import scala.collection.{GenSet, mutable}
import scala.annotation.tailrec
import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global
import rx.async._
import rx.async.Platform._

import scala.concurrent.duration._
import rx.async._
import rx.async.Platform._

import scala.concurrent.duration._


/**
  * Created by chris on 03.07.2016.
  */
case class CharmDB(map: Map[CharmRef, Charm])(implicit char: AtomImpl.Character) {

  def apply(charmRef: CharmRef): Charm = map(charmRef)

  def pathToPurchase(charmRef: CharmRef)(implicit char: AtomImpl.Character): Seq[AtomicExchange] = {
    //TODO: implement more complex paths than pure identity + charm purchase
    Seq(PurchaseCharm(charmRef))
  }

  def findRecursivePrecursors(origin: CharmRef): Set[CharmRef] = findPrecursorsTailRec(Set.empty, Set(origin))

  @tailrec private def findPrecursorsTailRec(found: Set[CharmRef], openToInvestigate: Set[CharmRef]): Set[CharmRef] = {
    if (openToInvestigate.nonEmpty) {
      def next = openToInvestigate.head
      def tail = openToInvestigate.tail
      def x: Set[CharmRef] = apply(next).requirement.precursorCharms
      findPrecursorsTailRec(found.++:(x), tail.++:(x))
    } else {
      found
    }
  }

  def findCategoryBelongingCharmsMinusSet(excludeThoseCharms: GenSet[CharmRef], category: Option[String] = None, minimalEssence: Option[Int] = None): Set[CharmRef] = {
    val filtered = category.map(co => map.values.filter(_.category.contains(co))).getOrElse(map.values).filter(c => minimalEssence.flatMap(e => c.essence.map(f => e < f)).getOrElse(true)).map(_.charmRef).toSet
    filtered.diff(excludeThoseCharms)
  }


  import CharmDB._

  val states: Dict[CharmRef, CharmPurchaseStatus] = Dict.apply(map.map(a => (a._1, Unreachable)))
  renewStatesAsync()

  //called once at start

  def stateOf(c: CharmRef): CharmPurchaseStatus = states.get(c).getOrElse(Illegal)

  import Helper.WidokVarHelpers._

  trait DebouncerEventable {
    def exec(): Unit
  }

  case object DebouncerEvent extends DebouncerEventable {
    override def exec(): Unit = triggerStateRecalcAsync()
  }

  case object NoEvent extends DebouncerEventable {
    override def exec(): Unit = println("called NOEVENT")
  }

  implicit val ctx: Ctx.Owner = Ctx.Owner.safe()
  val stateDebouncer = rx.Var[DebouncerEventable](NoEvent)
  val debounced = stateDebouncer.debounce(2000 millis)
  private val foreach = debounced.foreach(_.exec())


  private def triggerStateRecalcAsync()(implicit char: AtomImpl.Character): Unit = {
    println("Calculating all states new")
    states.keys$.foreach(a => Future {
      //println(s"Calculating state for $a")
      states.insertOrUpdate(a, calcState(a))
    })
  }

  def renewStatesAsync()(implicit char: AtomImpl.Character): Unit = Future {
    println("Triggering a state recalc event")
    stateDebouncer() = DebouncerEvent
  }

  private def calcState(c: CharmRef)(implicit char: AtomImpl.Character): CharmPurchaseStatus = {

    if (char.charms.contains(c)) {
      Bought
    } else {
      val charm = apply(c)
      implicit val db = this
      charm.requirement.eval()
    }
  }

}

object CharmDB {
  def default(implicit char: AtomImpl.Character): CharmDB = {
    val raw = Charm.defaultCharms.groupBy(_.charmRef)
    assert(raw.forall(_._2.size == 1))
    val processed = raw.mapValues(_.head)
    CharmDB(processed)
  }


  sealed trait CharmPurchaseStatus

  case object Bought extends CharmPurchaseStatus

  //already in posession
  case object DirectReachable extends CharmPurchaseStatus

  //charm is directly available for purchase
  case object ExtendedReachable extends CharmPurchaseStatus

  //charm can be purchased by first purchasing other charm (does not check if XP is enough for all of that!) other things need to be the same
  case object Unreachable extends CharmPurchaseStatus

  //includes higher essence and similar forbiddances
  case object Illegal extends CharmPurchaseStatus

  //guard element
}