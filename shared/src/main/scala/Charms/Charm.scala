package Charms

import AtomExal.AtomicExchange
import Requirement.{ByTraits, _}
import org.widok.ReadChannel

/**
  * Created by chris on 09.07.2016.
  */
case class Charm(charmRef: CharmRef //unique identifiier
                 , name: String
                 , typ: String //taken from enum
                 , duration: String
                 , cost: String
                 , book: Option[String] //TODO: combine with page
                 , page: Option[Int] //TODO: combine with book
                 , effect: String
                 , abilityFamily: Option[(String, String, Int)] //just there for visualization //TODO: move to defs and get from requirement
                 , essence: Option[Int] //just there for visualization //TODO: move to defs and get from requirement
                 , requirement: ByTraits.TraitableRequirement) {

  def category : Option[String] = {
      abilityFamily.map(a => a._2)
  }

  //maybe a transformed version with requirement as a ReadChannel based on Character Channels and Required or a lazy val?
  def wrappedRx(character : AtomImpl.Character)(implicit charmdb : CharmDB) : Charm.WrappedRx = new Charm.WrappedRx(character, this)
}

object Charm {

  class WrappedRx(character : AtomImpl.Character, val charm : Charm)(implicit charmdb : CharmDB) {
    lazy val alreadyPurchased : ReadChannel[Boolean] = character.rxCharms.contains(charm.charmRef)
    lazy val canBeDiretlyPurchased : ReadChannel[Boolean] = ???
    lazy val canBeIndirectlyPurchased : ReadChannel[Boolean] = ???
    lazy val pathToThis : ReadChannel[Seq[AtomicExchange]] = ???
  }

  def mock(i : Int) = Charm(
    CharmRef(i)
    , "Mock Charm  #" + i.toString
    , Supplemental.toString
    , "Instant"
    , "1wp"
    , Some("Core"), Some(351)
    , "Once per scene, the Solar may use a full Melee excellency by paying a single willpower. Can be reset by incapacitating an enemy with a Melee attack."
    , Some(("Melee", "Melee", 5))
    , Some(3)
    , ByTraits.RequireEssence(1).and(ByTraits.RequireAbility("Melee", "Melee", 5)).and(ByTraits.RequirePrecursorCharm(Set(CharmRef(100001), CharmRef(100003))))
  )

  sealed trait CharmTyp

  case object Reflexive extends CharmTyp

  case object Permanent extends CharmTyp

  case object Simple extends CharmTyp

  case object Supplemental extends CharmTyp

  def typvalues = Seq(Reflexive, Permanent, Simple, Supplemental)

  def charmTypes: Seq[String] = typvalues.map(_.toString)


  val excellentStrike: Charm = Charm(
    CharmRef(100001)
    , "Excellent Strike"
    , Supplemental.toString
    , "Instant"
    , "3m"
    , None, None
    , "Add 1 automatic success and reroll 1 until they don't appear."
    , Some(("Melee", "Melee", 2))
    , Some(1)
    , ByTraits.RequireEssence(1).and(ByTraits.RequireAbility("Melee", "Melee", 2))
  )

  val oneWeaponTwoBlows: Charm = Charm(
    CharmRef(100002)
    , "One Weapon, Two Blows"
    , Reflexive.toString
    , "Instant"
    , "7m"
    , None, None
    , "When a withering attack reduces an enemy below your initiative, immediately make a second attack."
    , Some(("Melee", "Melee", 2))
    , Some(1)
    , ByTraits.RequireEssence(1).and(ByTraits.RequireAbility("Melee", "Melee", 2)).and(ByTraits.RequirePrecursorCharm(Set(CharmRef(100001))))
  )

  val dippingSwallowDefense: Charm = Charm(
    CharmRef(100003)
    , "Dipping Swallow Defense"
    , Supplemental.toString
    , "Instant"
    , "2m"
    , None, None
    , "Ignore all penalties to parry defense. Grants one initiative per defended attack."
    , Some(("Melee", "Melee", 1)) //TODO: reimplement to two
    , Some(1)
    , ByTraits.RequireEssence(1).and(ByTraits.RequireAbility("Melee", "Melee", 1))
  )

  val perfectStrikeDiscipline: Charm = Charm(
    CharmRef(100004)
    , "Perfect Strike Discipline"
    , Supplemental.toString
    , "Instant"
    , "1wp"
    , Some("Core"), Some(351)
    , "Once per scene, the Solar may use a full Melee excellency by paying a single willpower. Can be reset by incapacitating an enemy with a Melee attack."
    , Some(("Melee", "Melee", 5))
    , Some(3)
    , ByTraits.RequireEssence(3).and(ByTraits.RequireAbility("Melee", "Melee", 5)).and(ByTraits.RequirePrecursorCharm(Set(CharmRef(100001), CharmRef(100003))))
  )

  val terrestrialCircleSorcery: Charm = mock(100005)

  val orderAffirmingBlow: Charm = mock(100006)

  val harmoniousAcademicMethodology: Charm = mock(100007)

  //TODO: def bottomlessWellspringApproach: Charm = ???

  //TODO: def loreInducingConcentration: Charm = ???

  //TODO: def truthRenderingGaze: Charm = ???

  //TODO: def heavenTurningCalculations: Charm = ???

  //TODO: def firstKnowledgesGrace: Charm = ???

  //TODO: def flowingMindPrana: Charm = ???

  val wiseArrow: Charm = mock(100008)

  //TODO: def tranceOfUnhesitatingSpeed: Charm = ???

  val craftsmanNeedsNoTools: Charm = mock(100009)

  //TODO: def areteShiftingPrana: Charm = ???

  //TODO: def sublimeTransferance: Charm = ???


  val defaultCharms = Seq(
    excellentStrike
    , oneWeaponTwoBlows
    , dippingSwallowDefense
    , perfectStrikeDiscipline
    , terrestrialCircleSorcery
    , orderAffirmingBlow
    , harmoniousAcademicMethodology
    , wiseArrow
    , craftsmanNeedsNoTools
  ) ++ (100 to 700).map(mock)


  assert(defaultCharms.map(_.charmRef).distinct.length == defaultCharms.length)
}