package Requirement

import AtomImpl.Character
import Charms.{CharmDB, CharmRef}

import Helper.WidokVarHelpers._

/**
  * Created by Christopher on 22.07.2016.
  */
abstract class BooleanExpressionImproved {
  def children : Set[BooleanExpressionImproved]
  protected def selfEval(childEvals : Set[Boolean], character : AtomImpl.Character)(implicit charmdb : CharmDB) : Boolean //only evaluate oneself, without children
  def eval(character : AtomImpl.Character)(implicit charmdb : CharmDB) : Boolean = {
    selfEval(evalChildren(character), character)
  }

  def evalChildren(character : AtomImpl.Character)(implicit charmdb : CharmDB) : Set[Boolean] = children.map(_.eval(character))

  def precursorCharms : Set[CharmRef] = Set.empty //TODO: tail call recursion for performance

  def and( b : BooleanExpressionImproved*) : BooleanExpressionImproved = AndImproved(b.+:(this).toSet)
  def not() : BooleanExpressionImproved = NotImproved(this)
}

case class AndImproved(children : Set[BooleanExpressionImproved])  extends BooleanExpressionImproved {
  override protected def selfEval(childEvals : Set[Boolean], character : AtomImpl.Character)(implicit charmdb : CharmDB): Boolean = childEvals.reduce((a, b) => a && b)
  override def precursorCharms = children.flatMap(_.precursorCharms)
}

object AndImproved {
  def apply(os: BooleanExpressionImproved*) : AndImproved = {
    AndImproved(os.toSet)
  }
}


case class NotImproved(a : BooleanExpressionImproved) extends BooleanExpressionImproved {

  override def eval(character: AtomImpl.Character)(implicit charmdb : CharmDB): Boolean = !a.eval(character)

  override def children: Set[BooleanExpressionImproved] = Set(a)

  override protected def selfEval(childEvals : Set[Boolean], character : AtomImpl.Character)(implicit charmdb : CharmDB): Boolean = true
}

case class AlwaysImproved() extends BooleanExpressionImproved {
  override def children: Set[BooleanExpressionImproved] = Set.empty

  override protected def selfEval(childEvals: Set[Boolean], character: Character)(implicit charmdb: CharmDB): Boolean = true
}
case class RequireEssenceImproved(minimal : Int) extends BooleanExpressionImproved {
  override def children: Set[BooleanExpressionImproved] = Set.empty

  override protected def selfEval(childEvals: Set[Boolean], character: Character)(implicit charmdb: CharmDB): Boolean = character.essence >= minimal
}

case class RequireAbilityImproved(abilitygroup : String, abilityfamily : String, minimal : Int) extends BooleanExpressionImproved {
  override def children: Set[BooleanExpressionImproved] = Set.empty

  override protected def selfEval(childEvals: Set[Boolean], character: Character)(implicit charmdb: CharmDB): Boolean = character.getAbilityRatingForCharm(abilitygroup, abilityfamily).exists(i => i >= minimal)
}

case class RequirePrecursorCharmImproved(previous : Set[CharmRef]) extends BooleanExpressionImproved {
  override def children: Set[BooleanExpressionImproved] = Set.empty
  override def precursorCharms = previous

  override protected def selfEval(childEvals: Set[Boolean], character: Character)(implicit charmdb: CharmDB): Boolean =  previous.flatMap(f => charmdb.findRecursivePrecursors(f)).forall(c => character.rxCharms.toBufSet.contains$(c))
}

//maybe exclude this to go with a typical acyclic graph instead of this standard exalted crap?
case class CharmsOfCategoryExcludingRecursivePrecursorImproved (number : Int, precursors: Set[CharmRef], category : Option[String] = None, minimalEssence : Option[Int] = None) extends BooleanExpressionImproved {
  override def children: Set[BooleanExpressionImproved] = Set.empty

  override protected def selfEval(childEvals: Set[Boolean], character: Character)(implicit charmdb: CharmDB): Boolean = {
    charmdb.findCategoryBelongingCharmsMinusSet(precursors.flatMap(c => charmdb.findRecursivePrecursors(c)), category, minimalEssence).size >= number
  }

}