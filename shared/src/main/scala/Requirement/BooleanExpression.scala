package Requirement

import Atomar.Path
import Character.CharacterSheet
import Charms.{CharmDB, CharmRef}

import scala.annotation.tailrec
import Helper.WidokVarHelpers._

/**
  * Created by chris on 03.07.2016.
  */
abstract class BooleanExpression {
 def eval(character : AtomImpl.Character)(implicit charmdb : CharmDB) : Boolean //TODO: tail call recursion for performance

  def precursorCharms : Set[CharmRef] = Set.empty //TODO: tail call recursion for performance


  def and( b : BooleanExpression) : BooleanExpression = And(this, b)
  def or( b : BooleanExpression) : BooleanExpression = Or(this, b)
  def xor( b : BooleanExpression) : BooleanExpression = Xor(this, b)
  def not() : BooleanExpression = Not(this)
}

//CONTAINERS

case class And(a : BooleanExpression, b : BooleanExpression) extends BooleanExpression {
  override def eval(character: AtomImpl.Character)(implicit charmdb : CharmDB): Boolean = a.eval(character) && b.eval(character)
}

case class Or(a : BooleanExpression, b : BooleanExpression) extends BooleanExpression {
  override def eval(character: AtomImpl.Character)(implicit charmdb : CharmDB): Boolean = a.eval(character) || b.eval(character)
}

case class Xor(a : BooleanExpression, b : BooleanExpression) extends BooleanExpression {
  override def eval(character: AtomImpl.Character)(implicit charmdb : CharmDB): Boolean = a.eval(character) != b.eval(character)
}

case class Not(a : BooleanExpression) extends BooleanExpression {
  override def eval(character: AtomImpl.Character)(implicit charmdb : CharmDB): Boolean = !a.eval(character)
}


//NORMAL

case class Always() extends BooleanExpression {
  override def eval(character: AtomImpl.Character)(implicit charmdb : CharmDB): Boolean = true
}

case class RequireEssence(minimal : Int) extends BooleanExpression {
  override def eval(character: AtomImpl.Character)(implicit charmdb : CharmDB): Boolean = character.essence >= minimal
}

case class RequireWillpower(minimal : Int) extends BooleanExpression {
  override def eval(character: AtomImpl.Character)(implicit charmdb : CharmDB): Boolean = character.willpowerRating >= minimal
}

case class RequireAbility(abilitygroup : String, abilityfamily : String, minimal : Int) extends BooleanExpression {
  override def eval(character: AtomImpl.Character)(implicit charmdb : CharmDB): Boolean = character.getAbilityRatingForCharm(abilitygroup, abilityfamily).getOrElse(0) >= minimal
}

case class RequirePrecursorCharm(previous : Set[CharmRef]) extends BooleanExpression {
  override def eval(character: AtomImpl.Character)(implicit charmdb : CharmDB): Boolean = {
    previous.flatMap(f => charmdb.findRecursivePrecursors(f)).forall(c => character.rxCharms.toBufSet.contains$(c))
  }
  override def precursorCharms = previous
}

//maybe exclude this to go with a typical acyclic graph instead of this standard exalted crap?
case class CharmsOfCategoryExcludingRecursivePrecursor (number : Int, precursors: Set[CharmRef], category : Option[String] = None, minimalEssence : Option[Int] = None) extends BooleanExpression {
  override def eval(character: AtomImpl.Character)(implicit charmdb : CharmDB): Boolean = {
    charmdb.findCategoryBelongingCharmsMinusSet(precursors.flatMap(c => charmdb.findRecursivePrecursors(c)), category, minimalEssence).size >= number
  }
}