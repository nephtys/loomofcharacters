package Requirement.ByTraits

import Charms.{CharmDB, CharmRef}
import Requirement.{ByTraits, _}
import Helper.WidokVarHelpers._

/**
  * Created by nephtys on 8/26/16.
  */
abstract class TraitableRequirement {
  def eval()(implicit charmdb : CharmDB, char : AtomImpl.Character) : CharmDB.CharmPurchaseStatus //TODO: tail call recursion for performance

  def precursorCharms : Set[CharmRef] = Set.empty //TODO: tail call recursion for performance


  def and( b : TraitableRequirement) : TraitableRequirement = ByTraits.And(this, b)
  def or( b : TraitableRequirement) : TraitableRequirement = ByTraits.Or(this, b)
  def xor( b : TraitableRequirement) : TraitableRequirement = ByTraits.Xor(this, b)
  def not() : TraitableRequirement = ByTraits.Not(this)
}
//CONTAINERS

case class And(a : TraitableRequirement, b : TraitableRequirement) extends TraitableRequirement {
  override def eval()(implicit charmdb : CharmDB, char : AtomImpl.Character): CharmDB.CharmPurchaseStatus = {
    if (a.eval() == CharmDB.DirectReachable && b.eval() == CharmDB.DirectReachable) {
      CharmDB.DirectReachable
    } else if (a.eval() == CharmDB.Unreachable || a.eval() == CharmDB.Unreachable) {
      CharmDB.Unreachable
    } else {
      CharmDB.ExtendedReachable
    }
  }
}

case class Or(a : TraitableRequirement, b : TraitableRequirement) extends TraitableRequirement {
  override def eval()(implicit charmdb : CharmDB, char : AtomImpl.Character): CharmDB.CharmPurchaseStatus = {
    if (a.eval() == CharmDB.DirectReachable || b.eval() == CharmDB.DirectReachable) {
      CharmDB.DirectReachable
    } else if (a.eval() == CharmDB.Unreachable && a.eval() == CharmDB.Unreachable) {
      CharmDB.Unreachable
    } else {
      CharmDB.ExtendedReachable
    }
  }
}

case class Xor(a : TraitableRequirement, b : TraitableRequirement) extends TraitableRequirement {
  override def eval()(implicit charmdb : CharmDB, char : AtomImpl.Character): CharmDB.CharmPurchaseStatus = {
    if (a.eval() == CharmDB.DirectReachable && b.eval() == CharmDB.DirectReachable) {
      CharmDB.DirectReachable
    } else if (a.eval() == CharmDB.Unreachable || a.eval() == CharmDB.Unreachable) {
      CharmDB.Unreachable
    } else {
      CharmDB.ExtendedReachable
    }
  }
}

case class Not(a : TraitableRequirement) extends TraitableRequirement {
  override def eval()(implicit charmdb : CharmDB, char : AtomImpl.Character): CharmDB.CharmPurchaseStatus = {
    if (a.eval() == CharmDB.Unreachable) {
      CharmDB.DirectReachable
    } else {
      CharmDB.Unreachable
    }
  }
}


//NORMAL

case class Always() extends TraitableRequirement {
  override def eval()(implicit charmdb : CharmDB, char : AtomImpl.Character): CharmDB.CharmPurchaseStatus = CharmDB.DirectReachable
}

case class Never() extends TraitableRequirement {
  override def eval()(implicit charmdb : CharmDB, char : AtomImpl.Character): CharmDB.CharmPurchaseStatus = CharmDB.Unreachable
}

case class RequireEssence(minimal : Int) extends TraitableRequirement {
  override def eval()(implicit charmdb : CharmDB, char : AtomImpl.Character): CharmDB.CharmPurchaseStatus = {
    if (char.essence < minimal) {
      CharmDB.Unreachable
    } else {
      CharmDB.DirectReachable
    }
  }
}

case class RequireWillpower(minimal : Int) extends TraitableRequirement {
  override def eval()(implicit charmdb : CharmDB, char : AtomImpl.Character): CharmDB.CharmPurchaseStatus =  {
    if (char.willpowerRating >= minimal) {
      CharmDB.DirectReachable
    } else {
      CharmDB.Unreachable
    }
  }
}

case class RequireAbility(abilitygroup : String, abilityfamily : String, minimal : Int) extends TraitableRequirement {
  override def eval()(implicit charmdb : CharmDB, char : AtomImpl.Character): CharmDB.CharmPurchaseStatus = {
    if (char.getAbilityRatingForCharm(abilitygroup, abilityfamily).getOrElse(0) >= minimal) {
      CharmDB.DirectReachable
    } else {
      CharmDB.Unreachable
    }
  }
}

case class RequirePrecursorCharm(previous : Set[CharmRef]) extends TraitableRequirement {
  override def eval()(implicit charmdb : CharmDB, char : AtomImpl.Character): CharmDB.CharmPurchaseStatus = {
    if (previous.flatMap(f => charmdb.findRecursivePrecursors(f)).forall(c => char.rxCharms.toBufSet.contains$(c))) {
      CharmDB.DirectReachable
    } else {
      CharmDB.ExtendedReachable
    }
  }
  override def precursorCharms = previous
}

//maybe exclude this to go with a typical acyclic graph instead of this standard exalted crap?
case class CharmsOfCategoryExcludingRecursivePrecursor (number : Int, precursors: Set[CharmRef], category : Option[String] = None, minimalEssence : Option[Int] = None) extends TraitableRequirement {
  override def eval()(implicit charmdb : CharmDB, char : AtomImpl.Character): CharmDB.CharmPurchaseStatus = {
    if (charmdb.findCategoryBelongingCharmsMinusSet(precursors.flatMap(c => charmdb.findRecursivePrecursors(c)), category, minimalEssence).size >= number) {
      CharmDB.DirectReachable
    } else {
      CharmDB.ExtendedReachable
    }
  }
}