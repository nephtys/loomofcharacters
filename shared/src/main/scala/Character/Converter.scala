package Character

import rx.{Rx, Var}

/**
  * Created by chris on 03.07.2016.
  */
object Converter {
  val a = Var(1)
  val b = Var(2)

  val x = Rx { a() + b() }

  println(x.now)

  a() = 4

  println(x.now)

  def load(): Unit = println("Hello World")
}
