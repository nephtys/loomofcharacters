package Character

import AtomExal.{AbilityType, Caste}
import Charms.CharmRef
import Experience.{General, Solar, Special}
import Traits._

/**
  * Created by chris on 03.07.2016.
  */
case class CharacterSheet() {


  def willpowerDots : Int = 5
  def willpowerPoints : Int = 4



  def xpSpentTotal : (General, Solar, Special) = (General(55), Solar(12), Special(13))
  def xpGainedTotal : (General, Solar, Special) = (General(60), Solar(22), Special(19))

  def specialties : Set[Specialty] = Set(Specialty("Craft", "Ships"), Specialty("Archery", "Bows"), Specialty("Lore", "6th Century History"))

  def attributes : Map[String, Int] = Map("Strength" -> 1, "Dexterity" -> 5, "Stamina" -> 3, "Charisma" -> 5, "Manipulation" -> 1, "Appearance" -> 1, "Perception" -> 3, "Intelligence" -> 5, "Wits" -> 3)
  assert(attributes.toSeq.size == 9)
  assert(attributes.forall(a => a._2 >= 1))


  def abilityFamilies : Set[AbilityFamily] = AbilityFamily.all

  def abilities : Map[Ability, (AbilityType.EnumVal, Int)] = abilityFamilies.flatMap(a => a.ratedAbilities.toSeq.map(s => (s._1, (a.typ,s._2)))).toMap

  def caste : Caste.CasteTyp = Caste.Twilight

  def anima : String = "Legen - wait for it - dary"

  def concept : String = "Awesome Testcharacter"

  def name : String = "Captain McAwesome"

  def player : String = "No one"

  def supernal : String = abilityFamilies.filter(_.typ == AbilityType.Supernal).map(_.name).headOption.getOrElse("N/A") //shoud determine this based on abilities

  def limitTrigger : String = "If something is not cool enough"

  def commitedEssence : Int = 5

  def limit : Int = 3

  def health : HealthTrack = HealthTrack.default

  def charms : Set[CharmRef] = Set.empty[CharmRef]
}
