package AtomExal

import AtomExal.Caste.CasteTyp
import Atomar.MutableState

/**
  * Created by chris on 16.07.2016.
  */
trait Characterable extends MutableState[Int]{

  //metastuff and limit


  //limit trigger
  def limitTrigger : String
  def setLimitTrigger(s : String) : Unit

  //name
  def name : String
  def setName(s : String) : Unit

  //player
  def player : String
  def setPlayer(s : String) : Unit

  //caste
  def caste : Option[CasteTyp]
  def setCaste(c : Option[CasteTyp]) : Unit

  //anima
  def anima : String
  def setAnima(s : String) : Unit

  //concept
  def concept : String
  def setConcept(s : String) : Unit


}
