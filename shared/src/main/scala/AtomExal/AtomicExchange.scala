package AtomExal

import AtomExal.CaseClasses.Experience
import AtomExal.Caste.CasteTyp
import AtomImpl.Changes.Stacks
import Charms.CharmRef

/**
  * Created by chris on 16.07.2016.
  */


import upickle.default._

sealed trait Atomic[T] {
  def designedStack : T
}

sealed trait AtomicExchange extends Atomic[Int]

case class AbilityAddChange(group: String, family: String, newAbility: String) extends AtomicExchange {

  override def designedStack: Int = Stacks.Ability
}



case class AbilityRatingChange(group: String, family: String, ability: String, from: Int, to: Int) extends AtomicExchange {
  assert(from - to <= 1 && from - to >= -1)

  override def designedStack: Int = Stacks.Ability
}

/**
  * Created by Christopher on 17.07.2016.
  */
case class AbilityRemoveChange(group: String, family: String, abilityToDelete: String) extends AtomicExchange {

  override def designedStack: Int = Stacks.Ability
}


case class AttributeIncreaseByOneChange(attribute: String, i: Int) extends AtomicExchange {
  assert(i >= -1 && i <= 1)

  override def designedStack: Int = Stacks.Attribute
}


/**
  * Created by Christopher on 17.07.2016.
  */
case class AbilityTypeChange(group: String, from: AbilityType.EnumVal, to: AbilityType.EnumVal) extends AtomicExchange {

  override def designedStack: Int = Stacks.Ability
}


/**
  * Created by nephtys on 8/14/16.
  */
case class ItemCommentChange(id: Long, from: String, to: String) extends AtomicExchange {
  override def designedStack: Int = Stacks.Item

}


/**
  * Created by nephtys on 8/14/16.
  */
case class MeritAddChange(category: String) extends AtomicExchange {
  override def designedStack: Int = Stacks.Merit

}


/**
  * Created by nephtys on 8/14/16.
  */
case class MeritRemoveChange(id: Long) extends AtomicExchange {
  override def designedStack: Int = Stacks.Merit

}


/**
  * Created by Christopher on 17.07.2016.
  */
case class NameChange(from: String, to: String) extends AtomicExchange {
  override def designedStack: Int = Stacks.Strings
}

case class ConceptChange(from: String, to: String) extends AtomicExchange {
  override def designedStack: Int = Stacks.Strings
}


case class AnimaChange(from: String, to: String) extends AtomicExchange {
  override def designedStack: Int = Stacks.Strings
}

case class PlayerChange(from: String, to: String) extends AtomicExchange {
  override def designedStack: Int = Stacks.Strings
}


/**
  * Created by Christopher on 17.07.2016.
  */
case class PurchaseCharm(ref: CharmRef) extends AtomicExchange {
  override def designedStack: Int = Stacks.Charm
}

/**
  * Created by chris on 24.07.2016.
  */
case class WillpowerIncreaseByOne(increase: Boolean) extends AtomicExchange {
  override def designedStack: Int = Stacks.Willpower
}


/**
  * Created by Christopher on 18.07.2016.
  */
case class SpecialtyAddChange(group: String, family: String, specialty: String) extends AtomicExchange {
  override def designedStack: Int = Stacks.Specialty
}


/**
  * Created by nephtys on 8/14/16.
  */
case class MeritRatingChange(id: Long, rating: Int) extends AtomicExchange {
  assert(rating == 1 || rating == -1)

  override def designedStack: Int = Stacks.Merit
}


/**
  * Created by nephtys on 8/14/16.
  */
case class MeritNameChange(id: Long, from: String, to: String) extends AtomicExchange {
  override def designedStack: Int = Stacks.Merit
}


/**
  * Created by nephtys on 8/14/16.
  */
case class ItemRemoveChange(id: Long) extends AtomicExchange {
  override def designedStack: Int = Stacks.Item

}


/**
  * Created by nephtys on 8/14/16.
  */
case class ItemAddChange(title: String, isWeapon: Boolean, isArmor: Boolean, soak: Int, damage: Int, accuracy: Int) extends AtomicExchange {
  override def designedStack: Int = Stacks.Item
}


/**
  * Created by chris on 24.07.2016.
  */
case class IntimacyAddChange(title: String, level: String) extends AtomicExchange {
  override def designedStack: Int = Stacks.Intimacy
}


/**
  * Created by nephtys on 8/19/16.
  */
case class ExperienceChange(reason: String, increment: Experience) extends AtomicExchange {
  override def designedStack: Int = Stacks.Experience
}


/**
  * Created by Christopher on 17.07.2016.
  */
case class CasteChange(from: Option[CasteTyp], to: Option[CasteTyp]) extends AtomicExchange {
  override def designedStack: Int = Stacks.Costless
}

