package AtomExal

/**
  * Created by chris on 03.07.2016.
  */
object AbilityType {
  sealed trait EnumVal
  case object Normal extends EnumVal
  case object Caste extends EnumVal
  case object Favored extends EnumVal
  case object Supernal extends EnumVal
  val types = Seq(Normal, Caste, Favored, Supernal)
  val typesString = types.map(a => (a.toString, a)).toMap
  val typesS = types.map(_.toString)
  def stringToCaste(s : Option[String]) : Option[EnumVal] = s.filter(s => s.length > 0).flatMap(p => typesString.get(p))
  def casteToString(c : Option[EnumVal]) : Option[String] = c.map(_.toString)
  //def apply(c : Option[EnumVal]) = casteToString(c)
  //def apply(s : Option[String]) = stringToCaste(s)
}
