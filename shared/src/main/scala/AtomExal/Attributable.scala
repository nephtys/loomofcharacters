package AtomExal

import AtomExal.CaseClasses.Attribute
import Atomar.MutableState

/**
  * Created by chris on 16.07.2016.
  */
trait Attributable extends MutableState[Int]{


  //attributes

  def setAttribute(newAttribute : Attribute) : Unit
  def attributes : Seq[Attribute]
}
