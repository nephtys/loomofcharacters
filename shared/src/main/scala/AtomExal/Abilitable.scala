package AtomExal

import AtomExal.CaseClasses.Abilities.AbilityGroup
import AtomExal.CaseClasses.Specialty
import Atomar.MutableState

/**
  * Created by chris on 16.07.2016.
  */
trait Abilitable extends MutableState[Int]{



  //one ability group is immutable, but the rest is mutable

  def setAbilityGroup(newGroup : AbilityGroup) : Unit
  def abilityGroups : Seq[AbilityGroup]


  def getAbilityRatingForCharm(groupName : String, familyName : String) : Option[Int] = {
    abilityGroups.filter(_.group == groupName).flatMap(g => g.families).filter(_.family == familyName).flatMap(_.abilities).map(_.rating).sorted.lastOption
  }

  //specialties
  def specialties : Seq[Specialty]
  def addSpecialty(specialty : Specialty) : Unit
  def removeSpecialty(specialty : Specialty) : Unit

}
