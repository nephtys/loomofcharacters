package AtomExal

import AtomExal.CaseClasses.Intimacies.{Intimacy, IntimacyStrength}

/**
  * Created by chris on 16.07.2016.
  */
trait Intimicable {


  //intimacies
  def setIntimacyTo(str : String, strength : Option[IntimacyStrength.IntimacyStrengthEnumVal]) //None deletes the given intimacy
  def intimacies : Seq[Intimacy]



}
