package AtomExal.CaseClasses.Abilities

import AtomExal.AbilityType


/**
  * Created by Christopher on 16.07.2016.
  */
case class AbilityGroup(group : String, families : Seq[AbilityFamily], typ : AbilityType.EnumVal) {

  def addAbility(family: String, newability : String, removeinstead : Boolean) : AbilityGroup = {
    val index =families.indexWhere(p => p.family == family)
    val old = families(index)
    AbilityGroup(group, families.updated(index, old.addAbility(newability, removeinstead)), typ)
  }

  def setAbilityTo(ability : String, family : String, newrating : Int) : AbilityGroup = {
    val index =families.indexWhere(p => p.family == family)
    val old = families(index)
    AbilityGroup(group, families.updated(index, old.setAbilityTo(ability, newrating)), typ)
  }

  def setTypeTo(t : AbilityType.EnumVal) = this.copy(typ = t)

  def similar (m : AbilityGroup) : Boolean = m.group == this.group
}
