package AtomExal.CaseClasses.Abilities


/**
  * Created by Christopher on 16.07.2016.
  */
case class AbilityFamily(family : String, abilities : Seq[SingleAbility], fixed : Boolean) {
  def addAbility(newability: String, removeinstead : Boolean) : AbilityFamily = {
    if (removeinstead) {
      AbilityFamily(family, abilities.filterNot(_.title == newability), fixed)
    } else {
      if (!abilities.exists(_.title == newability.trim())) {
        AbilityFamily(family, abilities.+:(SingleAbility(newability, 0)), fixed)
      } else {
        this
      }
    }
  }

  assert(abilities.size == 1 || !fixed)

  def setAbilityTo(ability : String, newrating : Int) : AbilityFamily =  {
    val index = abilities.indexWhere(p => p.title == ability)
    val old = abilities(index)
    AbilityFamily(family, abilities.updated(index, old.setTo(newrating)), fixed)
  }

  def similar(m : AbilityFamily) = m.family == this.family
}
