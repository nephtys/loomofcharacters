package AtomExal.CaseClasses.Abilities

/**
  * Created by Christopher on 16.07.2016.
  */
case class SingleAbility(title : String, rating : Int) {

  def similar(m : SingleAbility) = m.title == this.title
  def setTo(i : Int) : SingleAbility = SingleAbility(title, i)
}
