package AtomExal.CaseClasses

import AtomExal.CaseClasses.Abilities.AbilityFamily

/**
  * Created by Christopher on 16.07.2016.
  */
case class Specialty(group : String, family : String, title : String) {

}
