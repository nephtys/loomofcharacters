package AtomExal.CaseClasses.Items

import java.util.concurrent.atomic.AtomicLong

import org.widok.Buffer

/**
  * Created by Christopher on 16.07.2016.
  */
case class Item(
    id : Long
   ,title : String = """"""
  , isArmor : Boolean = false
  , isWeapon : Boolean = false
  , evasionModifier : Int = 0
  , parryModifier : Int = 0
  , mobilityPenalty : Int = 0
  , soak : Int = 0
  , hardness : Int = 0
  , range : String = Item.ranges(0)
  , damage : Int = 0
  , accuracy : Int = 0
  , tags : Seq[String] = Seq.empty
  , overwhelming : Int = 0
  , commitmentCost : Int = 0
  , comment : String = """"""
               ) {
}


object Item {
  private val _possibleRanges = Seq("Close", "Short", "Medium", "Long", "Extreme", "Special")
  val ranges = Buffer.from(_possibleRanges)

  private val al =  new AtomicLong(0)
  def newID() = al.getAndIncrement()
}