package AtomExal.CaseClasses

import java.util.concurrent.atomic.AtomicLong

import org.widok.Buffer


case class Merit(category : String, name : String, rating : Int, id : Long) {


}

object Merit {
  private val _possibleCategories = Seq("Artifact", "Backing", "Contacts", "Ressources", "Retainer", "Allies")
  val categories = Buffer.from(_possibleCategories.sorted)

  private val al =  new AtomicLong(0)
  def newID() = al.getAndIncrement()
}