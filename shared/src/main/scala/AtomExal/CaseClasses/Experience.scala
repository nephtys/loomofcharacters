package AtomExal.CaseClasses

/**
  * Created by Christopher on 16.07.2016.
  */
case class Experience(cat : Int, amount : Int) extends Atomar.Cost {

}

object Experience {
  def defaultSetTotal = Seq((GENERALXP, 0), (SOLARXP, 0), (SPECIALXP, 0), (BONUSPOINT, 18), (MERITPOINTS, 10), (CHARMPOINTS, 15),
  (ATTRIBUTEPOINTS, 18), (ABILITYPOINTS, 28), (SPECIALTYPOINTS, 4), (ABILITYTYPECASTEPOINTS, 4), (ABILITYTYPEFAVOREDPOINTS, 5), (ABILITYTYPESUPERNALPOINTS, 1)).map(a => (a._1, Experience(a._1, a._2)))
  def defaultSetSpent = Seq((GENERALXP, 0), (SOLARXP, 0), (SPECIALXP, 0), (BONUSPOINT, 0), (MERITPOINTS, 0), (CHARMPOINTS, 0),
    (ATTRIBUTEPOINTS, 0), (ABILITYPOINTS, 0), (SPECIALTYPOINTS, 0), (ABILITYTYPECASTEPOINTS, 0), (ABILITYTYPEFAVOREDPOINTS, 0), (ABILITYTYPESUPERNALPOINTS, 0)).map(a => (a._1, Experience(a._1, a._2)))

  assert(defaultSetSpent.size == 12)


  val GENERALXP = 9
  val SOLARXP = 10
  val SPECIALXP = 11
  val BONUSPOINT = 8
  val MERITPOINTS = 6
  val CHARMPOINTS = 7
  val ATTRIBUTEPOINTS = 0
  val ABILITYPOINTS = 4
  val SPECIALTYPOINTS = 5
  val ABILITYTYPECASTEPOINTS = 1
  val ABILITYTYPEFAVOREDPOINTS = 2
  val ABILITYTYPESUPERNALPOINTS = 3


  def showIfFull(cat : Int) : Boolean = cat match {
    case SOLARXP => true
    case SPECIALXP => true
    case GENERALXP => true
    case _ => false
  }

  def toString(cat : Int) : String = cat match {
    case 9 => "GENERALXP"
    case 10 => "SOLARXP"
    case 11 => "SPECIALXP"
    case 8 => "BONUSPOINT"
    case 6 => "MERITPOINTS"
    case 7 => "CHARMPOINTS"
    case 0 => "ATTRIBUTEPOINTS"
    case 4 => "ABILITYPOINTS"
    case 5 => "SPECIALTYPOINTS"
    case 1 => "ABILITYTYPECASTEPOINTS"
    case 2 => "ABILITYTYPEFAVOREDPOINTS"
    case 3 => "ABILITYTYPESUPERNALPOINTS"
    case _ => "N/A"
  }

  val types : Seq[String] = (0 to 11).map(i => toString(i))
  private val typesMapped = types.zipWithIndex.toMap

  def fromString(s : String) : Int = typesMapped.getOrElse(s, GENERALXP)

}