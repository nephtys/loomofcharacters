package AtomExal.CaseClasses

/**
  * Created by Christopher on 16.07.2016.
  */
case class Attribute(name : String, rating : Int, category : String, line : String) {

  def similar(m : Attribute) = m.name == this.name

  def setRating(i : Int) = copy(rating = i)
}

object Attribute {
  val Strength = "Strength"
  val Dexterity = "Dexterity"
  val Stamina = "Stamina"
  val Charisma = "Charisma"
  val Manipulation = "Manipulation"
  val Appearance = "Appearance"
  val Perception = "Perception"
  val Intelligence = "Intelligence"
  val Wits = "Wits"
  val Power = "Power"
  val Finesse = "Finesse"
  val Resistance = "Resistance"
  val Physical = "Physical"
  val Social = "Social"
  val Mental = "Mental"

  private val rawattributes = Seq((Strength, Physical, Power) , (Dexterity, Physical, Finesse) , (Stamina, Physical, Resistance) , (Charisma, Social, Power) , (Manipulation, Social, Finesse) , (Appearance, Social, Resistance) , (Perception, Mental, Power) , (Intelligence, Mental, Finesse) , (Wits, Mental, Resistance) ).groupBy(_._3)
  private val attributes = rawattributes(Power) ++ rawattributes(Finesse) ++ rawattributes(Resistance)

  def default = attributes.map(a => Attribute(a._1, 1, a._2, a._3))
}