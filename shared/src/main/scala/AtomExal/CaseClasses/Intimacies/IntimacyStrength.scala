package AtomExal.CaseClasses.Intimacies

/**
  * Created by Christopher on 16.07.2016.
  */
object IntimacyStrength {
  sealed trait IntimacyStrengthEnumVal
  case object Minor extends IntimacyStrengthEnumVal
  case object Major extends IntimacyStrengthEnumVal
  case object Defining extends IntimacyStrengthEnumVal
  def values = Seq(Minor, Major, Defining)
  def valuesS = values.map(_.toString)
  def toVal(s : String) : Option[IntimacyStrengthEnumVal] = {
    val index = valuesS.indexOf(s)
    if (index >= 0 && index < valuesS.length) {
      Some(values(index))
    } else {
      None
    }
  }

}
