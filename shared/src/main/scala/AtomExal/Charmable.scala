package AtomExal

import Atomar.MutableState
import Charms.CharmRef

/**
  * Created by chris on 16.07.2016.
  */
trait Charmable extends MutableState[Int]{

  //charms
  def addCharm(charmRef : CharmRef) : Unit
  def removeCharm(charmRef : CharmRef) : Unit
  def charms : Seq[CharmRef]

}
