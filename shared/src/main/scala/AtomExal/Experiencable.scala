package AtomExal

import AtomExal.CaseClasses.Experience
import Atomar.MutableState

/**
  * Created by chris on 16.07.2016.
  */
trait Experiencable extends MutableState[Int]{

  def charGenFinished : Boolean
  def setCharGenFinished(b : Boolean)

  def bonusPointsOn : Boolean
  def setBonusPointsOn(b : Boolean)

  //experience and free points
  def xpTotal(cat : Int) : Experience
  def xpSpent(cat : Int) : Experience
  def setTotalXp(cat : Int, xp : Experience) : Unit
  def setSpentXp(cat : Int, xp : Experience) : Unit


  def essence : Int
}
