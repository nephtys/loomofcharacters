package AtomExal

import AtomExal.CaseClasses.Items.Item
import Atomar.MutableState

/**
  * Created by chris on 16.07.2016.
  */
trait Itemable extends MutableState[Int]{

  //items, weapons, armor
  def removeItem(item : Item)
  def addItem(item : Item)
  def items : Seq[Item]

}
