package AtomExal

import Atomar.MutableState

/**
  * Created by chris on 16.07.2016.
  */
trait Willable extends MutableState[Int]{

  //willpower
  def setWillpowerRating(i : Int) : Unit
  def willpowerRating : Int
}
