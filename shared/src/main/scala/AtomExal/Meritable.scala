package AtomExal

import AtomExal.CaseClasses.Merit
import Atomar.MutableState

/**
  * Created by chris on 16.07.2016.
  */
trait Meritable extends MutableState[Int]{

  //merits
  def setMerit(newmerit : Merit)
  def removeMerit(oldmerit : Merit)
  def merits : Seq[Merit]
}
