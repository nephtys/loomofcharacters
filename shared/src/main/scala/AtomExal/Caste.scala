package AtomExal

/**
  * Created by chris on 03.07.2016.
  */
object Caste {
  sealed trait CasteTyp
  case object Dawn extends CasteTyp
  case object Zenith extends CasteTyp
  case object Twilight extends CasteTyp
  case object Night extends CasteTyp
  case object Eclipse extends CasteTyp
  val castes = Seq(Dawn, Zenith, Twilight, Night, Eclipse)
  val castesString = castes.map(a => (a.toString))
  val castesMap = castes.map(a => (a.toString, a)).toMap
  def stringToCaste(s : Option[String]) : Option[CasteTyp] = s.filter(s => s.length > 0).flatMap(p => castesMap.get(p))
  def casteToString(c : Option[CasteTyp]) : Option[String] = c.map(_.toString)
}
