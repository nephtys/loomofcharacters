package Model.ChangeImplementation

import Model._
import Viewmodel.{Ability, AbilityFamily, Character}

/**
  * Created by chris on 16.07.2016.
  */
case class AddNewAbility(abilityfamily : AbilityFamily, newability : String, cost : Cost = NoneCost) extends  CombinedChange{
  override def isAllowed()(implicit charmdb: CharmDatabase, character: ModelCharacter): Boolean = {
    true
  }

  override def writeToVM()(implicit char: Character): Unit = {
    abilityfamily.abilities.+=(new Ability() {title.:=(newability)})
  }

  override def writeToModel()(implicit char: ModelCharacter): Unit = {
    println("writing AddNewAbility to Model")
    //TODO: implement
  }

  override def undoModel()(implicit char: ModelCharacter): Unit = ???

  override def calculateCosts()(implicit charmdb: CharmDatabase, character: ModelCharacter): Seq[CombinedChange] = {
    if (abilityfamily.abilities.get.exists(a => a.title.get.trim() == newability)) {
      Seq.empty
    } else {
      val c = if (charmGen) {
        BPCost(0)
      } else {
        ExpCost(Seq(0, 0, 0))
      }
      Seq(AddNewAbility(abilityfamily, newability, c))
    }
  }

  override def undoVM()(implicit char: Character): Unit = ???
}
