package Model.ChangeImplementation

import AtomExal.AbilityType
import Model._
import Viewmodel.{AbilityGroup, Character}

/**
  * Created by chris on 16.07.2016.
  */
case class SetAbilityType(abilityGroup : AbilityGroup, typ : Option[AbilityType.EnumVal], cost : Cost = NoneCost) extends  CombinedChange {
  println(s"Generating this: ${this.toString}")

  override def isAllowed()(implicit charmdb: CharmDatabase, character: ModelCharacter): Boolean = {
    println("is allowed")
    character.costApplicable(cost)
  }

  override def writeToVM()(implicit char: Character): Unit = abilityGroup.typ.:=(typ.map(_.toString))

  override def writeToModel()(implicit char: ModelCharacter): Unit = {
    char.applyCost(cost)
  }

  override def undoModel()(implicit char: ModelCharacter): Unit = ???

  override def calculateCosts()(implicit charmdb: CharmDatabase, character: ModelCharacter): Seq[CombinedChange] = {
    println(s"calculating cost for ${this.toString}")
    (if (charmGen) {
      typ match {
        case None => Seq()
        case Some(AbilityType.Supernal) => Seq(PointCost(1, ModelCharacter.ABILITYTYPESUPERNALPOINTS))
        case Some(AbilityType.Favored) => Seq(PointCost(1, ModelCharacter.ABILITYTYPEFAVOREDPOINTS))
        case Some(AbilityType.Caste) => Seq(PointCost(1, ModelCharacter.ABILITYTYPECASTEPOINTS))
        case Some(AbilityType.Normal) => Seq(PointCost(0, ModelCharacter.ABILITYTYPECASTEPOINTS))
        case _ => ???
      }
    } else {
      Seq()
    }).map( c => SetAbilityType(abilityGroup, typ, c))
  }

  override def undoVM()(implicit char: Character): Unit = ???
}
