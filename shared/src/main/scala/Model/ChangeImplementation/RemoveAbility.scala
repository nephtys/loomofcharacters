package Model.ChangeImplementation

import Model._
import Viewmodel.{Ability, AbilityFamily, Character}

/**
  * Created by chris on 16.07.2016.
  */
case class RemoveAbility(abilityfamily : AbilityFamily, abilityToDelete : Ability, cost : Cost = NoneCost) extends  CombinedChange{

  override def isAllowed()(implicit charmdb: CharmDatabase, character: ModelCharacter): Boolean = {
    abilityToDelete.rating.get == 0
  }

  override def writeToVM()(implicit char: Character): Unit = {
    abilityfamily.abilities.-=(abilityToDelete)
  }

  override def writeToModel()(implicit char: ModelCharacter): Unit = {
    println("writing RemoveAbility to Model")
    //TODO: implement
  }

  override def undoModel()(implicit char: ModelCharacter): Unit = ???

  override def calculateCosts()(implicit charmdb: CharmDatabase, character: ModelCharacter): Seq[CombinedChange] = {
    if (abilityToDelete.rating.get == 0) {
      val c = if (charmGen) {
        BPCost(0)
      } else {
        ExpCost(Seq(0, 0, 0))
      }
      Seq(RemoveAbility(abilityfamily, abilityToDelete, c))
    } else {
      Seq.empty
    }
  }

  override def undoVM()(implicit char: Character): Unit = ???
}
