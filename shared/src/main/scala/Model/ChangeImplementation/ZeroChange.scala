package Model.ChangeImplementation

import Model._
import Viewmodel.Character

/**
  * Created by chris on 11.07.2016.
  */
case object ZeroChange extends CombinedChange {
  override val cost: Cost = NoneCost

  override def calculateCosts()(implicit charmdb: CharmDatabase, character: ModelCharacter): Seq[CombinedChange] = ???

  override def isAllowed()(implicit charmdb: CharmDatabase, character: ModelCharacter): Boolean = ???

  override def writeToVM()(implicit char: Character): Unit = ???

  override def writeToModel()(implicit char: ModelCharacter): Unit = ???

  override def undoModel()(implicit char: ModelCharacter): Unit = ???

  override def undoVM()(implicit char: Character): Unit = ???
}
