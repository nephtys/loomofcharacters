package Model.ChangeImplementation

import Model._
import Viewmodel.Character

/**
  * Created by chris on 09.07.2016.
  */
case class WillpowerIncrease(newrating : Int, cost : Cost = NoneCost) extends  CombinedChange{


  override def calculateCosts()(implicit charmdb: CharmDatabase, character: ModelCharacter): Seq[CombinedChange] = {
      if(character.charGenFinished) {
        ExpCosts.find(8).map((c : Cost) => WillpowerIncrease(newrating, c))
      } else {
        if(useBonusPoints && character.bonusPointsLeftToSpend >= 8) {
          Seq(WillpowerIncrease(newrating, BPCost(4)))
        } else {
          Seq.empty
        }
      }
  }

  override def isAllowed()(implicit charmdb: CharmDatabase, character: ModelCharacter): Boolean =  {
    if (cost == NoneCost) {
      false
    } else {
      if (character.charGenFinished) {
        if (cost.isInstanceOf[ExpCost]) {
          cost.asInstanceOf[ExpCost].pointsGeneral <= character.generalXPLeft && cost.asInstanceOf[ExpCost].pointsSolar <= character.solarXPLeft && cost.asInstanceOf[ExpCost].pointsSpecial <= character.specialXPLeft
        } else {
          false
        }
      } else if (cost.isInstanceOf[BPCost] ) {
        cost.asInstanceOf[BPCost].points <= character.bonusPointsLeftToSpend
      } else {
        false
      }
    }
  }

  override def writeToVM()(implicit char: Character): Unit = ???

  override def writeToModel()(implicit char: ModelCharacter): Unit = ???

  override def undoModel()(implicit char: ModelCharacter): Unit = ???

  override def undoVM()(implicit char: Character): Unit = ???
}
