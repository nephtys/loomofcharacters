package Model.ChangeImplementation

import Model.{NoneCost, _}
import Viewmodel.{Ability, Character}

/**
  * Created by chris on 16.07.2016.
  */
case class SetAbilityRating(isFavored : Boolean, ability : Ability, newrating : Int, cost : Cost = NoneCost) extends  CombinedChange{
  println(s"Created this: ${this.toString}")
  override def isAllowed()(implicit charmdb: CharmDatabase, character: ModelCharacter): Boolean = {
    newrating >= 0 && newrating <= 5 && (if(cost == NoneCost) true else character.costApplicable(cost))
  }

  override def writeToModel()(implicit char: ModelCharacter): Unit = {
    char.applyCost(cost)
    println("writing SetAbilityRating to Model")
    //TODO: implement
  }

  override def writeToVM()(implicit char: Character): Unit = {
    assert(cost != NoneCost)
    ability.rating.:=(newrating)
  }

  override def undoModel()(implicit char: ModelCharacter): Unit = ???

  override def calculateCosts()(implicit charmdb: CharmDatabase, character: ModelCharacter): Seq[CombinedChange] = {
    val cur = ability.rating.get
    println("Calculating cost for SetRating")
    println(s"charmGen $charmGen")
    val costs : Seq[Cost] = if (charmGen) {
      if (useBonusPoints) {
        //TODO does this differ between normal and caste/favored?
        Seq(BPCost(newrating - cur))
      } else {
        if (newrating <= 3) {
          println("new rating is under 3")
          Seq(PointCost(newrating - cur, ModelCharacter.ABILITYPOINTS))
        } else {
          Seq()
        }
      }
    } else {
      val amount : Int = ExpCosts.abilityCost(this.newrating, isFavored)
      println(s"Costs $amount")
      ExpCosts.find(amount, false)
    }

    costs.map(c => SetAbilityRating(isFavored, ability, newrating, c))
  }

  override def undoVM()(implicit char: Character): Unit = ???
}
