package Model

import scala.collection.mutable

/**
  * Created by chris on 09.07.2016.
  */
trait HybridLog {

  protected val seq : mutable.Buffer[CombinedChange] = mutable.Buffer.empty

  def append(change : CombinedChange) : Unit = seq.append(change)

  def last() = seq.last

  def toSeq = this.seq.toList.toSeq

}
