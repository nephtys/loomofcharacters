package Model

/**
  * Created by chris on 09.07.2016.
  */
trait CombinedChange {
  val cost : Cost

  def charmGen(implicit charmdb : CharmDatabase, character : ModelCharacter) : Boolean = !character.charGenFinished
  def useBonusPoints(implicit charmdb : CharmDatabase, character : ModelCharacter) : Boolean = character.bonusPointsToggledOn


  def isAllowed()(implicit charmdb : CharmDatabase, character : ModelCharacter) : Boolean
  def calculateCosts()(implicit charmdb : CharmDatabase, character : ModelCharacter) : Seq[CombinedChange]
  def writeToVM()(implicit char : Viewmodel.Character) : Unit
  def writeToModel()(implicit char : ModelCharacter) : Unit
  def undoModel()(implicit char : ModelCharacter) : Unit
  def undoVM()(implicit char : Viewmodel.Character)  : Unit
  

}
