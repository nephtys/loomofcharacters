package Model

import scala.collection.mutable

/**
  * Created by chris on 09.07.2016.
  */
trait ModelCharacter {
  import ModelCharacter._

  def willpowerRating: Int = 5

  def bonusPointsLeftToSpend : Int = exps(BONUSPOINT).total - exps(BONUSPOINT).spent

  def generalXPLeft : Int = exps(GENERALXP).total - exps(GENERALXP).spent
  def solarXPLeft : Int = exps(SOLARXP).total - exps(SOLARXP).spent
  def specialXPLeft : Int = exps(SPECIALXP).total - exps(SPECIALXP).spent

  def expLeft = Seq(generalXPLeft, solarXPLeft, specialXPLeft)

  var exps = (0 to 11).map(i => new Exp(13, 15)).
    updated(ABILITYPOINTS, new Exp(spent = 0, total =  28)).
    updated(BONUSPOINT, new Exp(spent = 0, total =  18)).
    updated(ABILITYTYPECASTEPOINTS, new Exp(spent = 0, total =  4)).
    updated(ABILITYTYPEFAVOREDPOINTS, new Exp(spent = 0, total =  5)).
    updated(ABILITYTYPESUPERNALPOINTS, new Exp(spent = 0, total =  1)).
    updated(SPECIALTYPOINTS, new Exp(spent = 0, total =  4))


  def charGenFinished: Boolean = false

  def bonusPointsToggledOn : Boolean = false

  def applyCost(c : Cost) : Unit = c match {
    case BPCost(i) => exps = exps.updated(BONUSPOINT, exps(BONUSPOINT).spend(i))
    case PointCost(i, cat) => exps = exps.updated(cat, exps(cat).spend(i))
    case ExpCost(seq) => seq.zipWithIndex.foreach(k => exps = exps.updated(GENERALXP + k._2, exps(GENERALXP + k._2).spend(k._1)))
    case NoneCost => println("NoneCost applied")
    case _ => ???
  }

  def costApplicable(c : Cost) : Boolean = c match {
    case BPCost(i) => (exps(BONUSPOINT).total - exps(BONUSPOINT).spent) >= i
    case PointCost(i, cat) => (exps(cat).total - exps(cat).spent) >= i
    case ExpCost(seq) => seq.zipWithIndex.forall(k => (exps(GENERALXP + k._2).total - exps(GENERALXP + k._2).spent) >= k._1)
    case NoneCost => true
    case _ => false
  }
}

object ModelCharacter {
  val GENERALXP = 9
  val SOLARXP = 10
  val SPECIALXP = 11
  val BONUSPOINT = 8
  val MERITPOINTS = 6
  val CHARMPOINTS = 7
  val ATTRIBUTEPOINTS = 0
  val ABILITYPOINTS = 4
  val SPECIALTYPOINTS = 5
  val ABILITYTYPECASTEPOINTS = 1
  val ABILITYTYPEFAVOREDPOINTS = 2
  val ABILITYTYPESUPERNALPOINTS = 3

}