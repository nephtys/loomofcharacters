package Model

/**
  * Created by chris on 09.07.2016.
  */
trait Cost {

}

case class BPCost(points : Int) extends Cost
case class PointCost(points : Int, category : Int) extends Cost //TODO: refactor into an enumeration


case class ExpCost(exp : Seq[Int]) extends Cost {
  assert(exp.length == 3)
  def pointsGeneral : Int = exp(0)
  def pointsSolar : Int = exp(1)
  def pointsSpecial : Int = exp(2)
}

case object NoneCost extends Cost


object ExpCosts {

  def abilityCost(rating : Int, isFavored : Boolean) : Int = {
    (0 to rating).map(i => abilcost(i, isFavored)).sum
  }

  private def abilcost(i : Int, isFavored : Boolean) = {
    if (i == 0) {
      0
    } else if (i == 1) {
      3
    } else {
       2 * (i - 1) - (if (isFavored) 1 else 0)
    }
  }

  def find(amountOfXP : Int, forSolarCharms : Boolean = true)(implicit char : ModelCharacter) : Seq[ExpCost] = {
    val combs : Seq[(Int, Int, Int)] = (0 to 2).flatMap(first => (1 to 2).map(secI => (first , (first + secI) % 3, 3 - ((first + secI) % 3) - first )))

    val res : Seq[Option[Seq[Int]]] = combs.map(q => {
      def f1 = q._1
      def f2 = q._2
      def f3 = q._3

      val a : Int = math.min(char.expLeft(f1), amountOfXP)
      val restA = amountOfXP - a
      val b : Int = math.min(char.expLeft(f2), restA)
      val restB = restA - b
      val c : Int = math.min(char.expLeft(f3), restB)
      val restC = restB - c
      val seq : Seq[(Int)] = if (f1 == 0) {
        if (f2 == 1) {
          Seq(a, b, c)
        } else {
          Seq(a, c, b)
        }
      } else if (f1 == 1) {
        if (f2 == 0) {
          Seq(b, a, c)
        } else {
          Seq(c, a, b)
        }
      } else {
        if (f2 == 0) {
          Seq(b,c,a)
        } else {
          Seq(c,b,a)
        }
      }

      if(restC == 0) {
        Some(seq)
      } else {
        None
      }
    })

    val r : Seq[ExpCost] = res.filter(_.isDefined).map(_.get).map(s => ExpCost(s))
    r
  }
}