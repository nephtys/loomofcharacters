package Model

/**
  * Created by Christopher on 13.07.2016.
  */
case class Exp(total : Int, spent : Int) {
def spend(i : Int) = {
  copy(spent = this.spent + i)
}
}
