package Atomar

/**
  * Created by Christopher on 16.07.2016.
  */
trait BaseState[T] {
  def toMutableState : MutableState[T]
}
