package Atomar

import scala.collection.mutable

/**
  * Created by Christopher on 16.07.2016.
  */
trait Graph[T] {
  val multiStack: Map[T, scala.collection.mutable.Stack[AtomExal.Atomic[T]]]
  val stackOrderLog : scala.collection.mutable.Stack[T] = mutable.Stack[T]()


  //def applyChange(c: AtomExal.Atomic[T]): Unit = {
    //multiStack.get(c.designedStack).foreach(_.push(c))
    //state.apply(c)
  //}
}
