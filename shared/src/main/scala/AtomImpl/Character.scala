package AtomImpl
import AtomExal._
import AtomExal.CaseClasses.Abilities.AbilityGroup
import AtomExal.CaseClasses.Intimacies.IntimacyStrength.IntimacyStrengthEnumVal
import AtomExal.CaseClasses.Intimacies.{Intimacy, IntimacyStrength}
import AtomExal.CaseClasses.Items.Item
import AtomExal.CaseClasses.{Attribute, Experience, Merit, Specialty}
import AtomExal.Caste.CasteTyp
import AtomImpl.Changes._
import Atomar.{Graph}
import Charms.{CharmDB, CharmRef}
import org.widok.{BufSet, Buffer, Var}

import scala.collection.mutable
import scala.collection.mutable.ListBuffer

/**
  * Created by Christopher on 17.07.2016.
  */
class Character
  extends Graph[Int]
    with AtomExal.Experiencable
    with AtomExal.Characterable
    with AtomExal.Attributable
    with AtomExal.Abilitable
    with AtomExal.Charmable
    with AtomExal.Willable
    with AtomExal.Intimicable
    with AtomExal.Meritable
    with AtomExal.Itemable {

  def canApplyChange(a: AtomicExchange)(implicit characterImplicit: Character): Boolean = {
    a match {

      //TODO implement XP and stuff

      case m: MeritAddChange => true
      case m: MeritRemoveChange => {
        val x = rxMerits.get.find(_.id == m.id).exists(_.rating == 0)
        println(s"Removing Merit: $x")
        x
      }
      case m: ExperienceChange => true
      case m: ItemAddChange => true
      case m: ItemRemoveChange => true
      case m: ItemCommentChange => true
      case m: MeritNameChange => true
      case m: MeritRatingChange => true
      case m: AttributeIncreaseByOneChange => true
      case m: AbilityTypeChange => true
      case m: NameChange => true
      case m: CasteChange => true
      case m: IntimacyAddChange => true
      case m: WillpowerIncreaseByOne => true
      case m: PlayerChange => true
      case m: AnimaChange => true
      case m: ConceptChange => true
      case m: AbilityRatingChange => true
      case m: AbilityAddChange => true
      case m: AbilityRemoveChange => true
      case m: SpecialtyAddChange => true
      case m: PurchaseCharm => true
      case _ => {
        println("Character canApplyChange unknown")
        ???
      }
    }

  }

  def applyChange(a: AtomicExchange)(implicit characterImplicit: Character, charmDB: CharmDB): Unit = {

    a match {


      case m: ExperienceChange => {
        println("Doing: " + m.toString)
        rxExpChanges.+=(m)

        val oldTotal = xpTotal(m.increment.cat).amount
        val oldSpent = xpSpent(m.increment.cat).amount
        val increase = m.increment.amount
        val newvalueTotal = if (increase > 0) {
          oldTotal + increase
        } else oldTotal
        val newvalueSpent = if (increase < 0) {
          oldSpent + ((-1) * increase)
        } else oldSpent
        val category = m.increment.cat
        val newXP = EXP(spent = Experience(category, newvalueSpent), total = Experience(category, newvalueTotal))

        println(s"Setting newXP : $newXP")

        rxExperience.update(f => {
          if (f.cat == category) {
            newXP
          } else {
            f
          }
        })

      }
      case m: ItemAddChange => {
        rxItems.+=(Item(id = Item.newID(), title = m.title, isArmor = m.isArmor, isWeapon = m.isWeapon))
      }
      case m: ItemRemoveChange => {
        rxItems.get.find(p => p.id == m.id).foreach(n => rxItems.-=(n))
      }
      case m: ItemCommentChange => {
        rxItems.update(f => if (f.id == m.id) {
          f.copy(comment = m.to)
        } else {
          f
        })
      }
      case m: MeritAddChange => {
        rxMerits.+=(Merit(m.category, "", 0, Merit.newID()))
        println(merits)
      }
      case m: MeritRemoveChange => {
        rxMerits.get.find(p => p.id == m.id).foreach(n => rxMerits.-=(n))
      }
      case m: MeritNameChange => {
        rxMerits.update(f => if (f.id == m.id) {
          f.copy(name = m.to)
        } else {
          f
        })
      }
      case m: MeritRatingChange => {
        rxMerits.update(f => if (f.id == m.id) {
          f.copy(rating = f.rating + m.rating)
        } else {
          f
        })
      }
      case AttributeIncreaseByOneChange(attribute, i) => {
        rxAttributes.update(f => if (f.name == attribute) {
          f.setRating(f.rating + i)
        } else {
          f
        })
        println(s"Changed $attribute by $i")
      }
      case AbilityTypeChange(group, from, to) => {
        rxAbilityGroups.update(f => {
          if (group == f.group) {
            println(s"changing group ${f.group} to type $to")
            f.setTypeTo(to)
          } else {
            println(s"keeping group ${f.group}")
            f
          }
        })
        println(s"Setted Type of $group from $from to $to")
      }
      case CasteChange(from, to) => {
        setCaste(to)
        println(s"Caste was changed to ${to.toString}")
      }
      case IntimacyAddChange(title, level) => {
        setIntimacyTo(title, IntimacyStrength.toVal(level))
        println(s"Have set Intimacy to $title at level $level")
      }
      case NameChange(from, to) => {
        setName(to)
        println(s"Changed Name from $from to $to")
      }
      case m: WillpowerIncreaseByOne => {
        setWillpowerRating(willpowerRating + (if (m.increase) 1 else -1))
        println("Increased Willpower by one")
      }
      case PlayerChange(from, to) => {
        setPlayer(to)
        println(s"Changed Player from $from to $to")
      }
      case ConceptChange(from, to) => {
        setConcept(to)
        println(s"Changed Concept from $from to $to")
      }
      case AnimaChange(from, to) => {
        setName(to)
        println(s"Changed Anima from $from to $to")
      }
      case PurchaseCharm(b) => {
        rxCharms.+=(b)
      }
      case SpecialtyAddChange(group, family, specialty) => {
        rxSpecialties.+=(Specialty(group, family, specialty))
      }
      case m: AbilityRatingChange => {
        rxAbilityGroups.update(f => {
          if (m.group == f.group) {
            println(s"changing group ${m.group} to $m")
            f.setAbilityTo(m.ability, m.family, m.to)
          } else {
            println(s"keeping group ${f.group}")
            f
          }
        })
      }
      case m: AbilityAddChange => {
        rxAbilityGroups.update(f => {
          if (m.group == f.group) {
            println(s"changing group ${m.group} to $m")
            f.addAbility(m.family, m.newAbility, removeinstead = false)
          } else {
            println(s"keeping group ${f.group}")
            f
          }
        })
      }
      case m: AbilityRemoveChange => {
        rxAbilityGroups.update(f => {
          if (m.group == f.group) {
            println(s"changing group ${m.group} to $m")
            f.addAbility(m.family, m.abilityToDelete, removeinstead = true)
          } else {
            println(s"keeping group ${f.group}")
            f
          }
        })
      }
      case _ => {
        println("Character applyChange unknown")
        ???
      }
    }
    improvisedLog.+=(a)
    charmDB.renewStatesAsync()
  }

  def unapplyChange(a: AtomicExchange)(implicit characterImplicit: Character, charmDB: CharmDB): Unit = {
    a match {
    case _ => {
      println("Character unapplyChange unknown")
      ???
    }
    }
    charmDB.renewStatesAsync()
    //TODO: remove last entry of this from log
    }

  def findChangePaths(seq : Seq[AtomicExchange])(implicit characterImplicit : Character) : Set[Seq[AtomicExchange]] = {
    println("findChangePaths called")
    Set(seq)
    //TODO: implement
  }


  val improvisedLog = ListBuffer[AtomicExchange]()


  case class EXP(spent : Experience, total : Experience) {
    def same : Boolean = spent.amount == total.amount
    def dif = total.amount - spent.amount
    def string = s"spent ${spent.amount} of ${total.amount}${if(same){""}else{", leaving "+dif.toString}}"
    def cat = spent.cat
  }

  val rxExperience = Buffer.from[EXP](Experience.defaultSetSpent.zip(Experience.defaultSetTotal).map(p => EXP(spent = p._1._2, total = p._2._2)).sortBy(_.cat))

  val rxExpChanges = Buffer[ExperienceChange]()

  //experience and free points
  override def xpTotal(cat: Int): Experience = rxExperience.get.apply(cat).total

  override def xpSpent(cat: Int): Experience = rxExperience.get.apply(cat).spent

  override def setTotalXp(cat: Int, xp: Experience): Unit = rxExperience.update(f => if (f.cat == cat) {
    f.copy(total = xp)
  } else {
    f
  })

  override def setSpentXp(cat: Int, xp: Experience): Unit = rxExperience.update(f => if (f.cat == cat) {
    f.copy(spent = xp)
  } else {
    f
  })


  val rxLimitTrigger = Var("")
  override def limitTrigger: String = rxLimitTrigger.get

  val rxCaste : Var[Option[CasteTyp]] = Var(None)
  override def setCaste(c: Option[CasteTyp]): Unit = rxCaste.:=(c)


  val rxAnima = Var("")
  override def setAnima(s: String): Unit = rxAnima.:=(s)

  val rxConcept = Var("")
  override def setConcept(s: String): Unit = rxConcept.:=(s)

  //concept
  override def concept: String = rxConcept.get

  //name
  override def name: String = rxName.get

  //caste
  override def caste: Option[CasteTyp] = rxCaste.get

  override def setName(s: String): Unit = rxName.:=(s)

  val rxPlayer = Var("Player is here")
  val rxName = Var("Name is here")
  //player
  override def player: String = rxPlayer.get

  //anima
  override def anima: String = rxAnima.get

  override def setLimitTrigger(s: String): Unit = rxLimitTrigger.:=(s)

  override def setPlayer(s: String): Unit = rxPlayer.:=(s)

  val rxCharGenFinished = Var(false)
  val rxBonusPointsOn = Var(false)
  override def charGenFinished: Boolean = rxCharGenFinished.get

  override def bonusPointsOn: Boolean = rxBonusPointsOn.get

  override def setCharGenFinished(b: Boolean): Unit = rxCharGenFinished.:=(b)

  override def setBonusPointsOn(b: Boolean): Unit = rxBonusPointsOn.:=(b)

  val rxAbilityGroups : Buffer[AbilityGroup] = Buffer()

  override def setAbilityGroup(newGroup: AbilityGroup): Unit = rxAbilityGroups.update(f => if(newGroup.similar(f)) {
    newGroup
  } else {
    f
  })

  val rxSpecialties : Buffer[Specialty] = Buffer()

  override def addSpecialty(specialty: Specialty): Unit = rxSpecialties.+=(specialty)

  override def removeSpecialty(specialty: Specialty): Unit = rxSpecialties.-=(specialty)

  //specialties
  override def specialties: Seq[Specialty] = rxSpecialties.get

  override def abilityGroups: Seq[AbilityGroup] = rxAbilityGroups.get


  val rxCharms : Buffer[CharmRef] = Buffer()


  //charms
  override def addCharm(charmRef: CharmRef): Unit = rxCharms.+=(charmRef)

  override def charms: Seq[CharmRef] = rxCharms.get

  override def removeCharm(charmRef: CharmRef): Unit = rxCharms.-=(charmRef)

  override val multiStack: Map[Int, mutable.Stack[AtomExal.Atomic[Int]]] = (Stacks.min to Stacks.max).map(i => (i, mutable.Stack[AtomExal.Atomic[Int]]())).toMap

  override def essence: Int = {
    val xp : Int = xpSpent(Experience.GENERALXP).amount
    if (xp >= 50) {
        if (xp >= 125) {
          if (xp >= 200) {
            if (xp >= 300) {
              5
            } else {
              4
            }
          } else {
            3
          }
        } else {
          2
        }
    } else {
      1
    }
  }

  val rxWillpower = Var(5)
  //willpower
  override def setWillpowerRating(i: Int): Unit = rxWillpower.:=(i)

  override def willpowerRating: Int = rxWillpower.get

  //intimacies

  val rxIntimacies = Buffer.apply[Intimacy]()

  override def setIntimacyTo(str: String, strength: Option[IntimacyStrengthEnumVal]): Unit = {
    strength.foreach(level => {
      if (intimacies.exists(p => p.title == str)) {
        //change
        rxIntimacies.update(f => {
          if (f.title == str) {
            Intimacy(str, level)
          } else {
            f
          }
        })
      } else {
        //add
        rxIntimacies.+=(Intimacy(str, level))
      }
    })
  }

  //None deletes the given intimacy
  override def intimacies: Seq[Intimacy] = rxIntimacies.get


  val rxAttributes = Buffer.from(Attribute.default)
  override def setAttribute(newAttribute: Attribute): Unit = {
    rxAttributes.update(f => if (f.similar(newAttribute)) {
      newAttribute
    } else {
      f
    })
  }

  override def attributes: Seq[Attribute] = rxAttributes.get



  val rxMerits = Buffer[Merit]()
  //merits
  override def setMerit(newmerit: Merit): Unit = ???

  override def removeMerit(oldmerit: Merit): Unit = ???

  override def merits: Seq[Merit] = rxMerits.get

  val rxItems = Buffer[Item]()
  //items, weapons, armor
  override def removeItem(item: Item): Unit = ???

  override def addItem(item: Item): Unit = ???

  override def items: Seq[Item] = rxItems.get
}