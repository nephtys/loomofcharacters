package AtomImpl

import AtomExal.CaseClasses.Abilities.{AbilityFamily, AbilityGroup, SingleAbility}
import AtomExal.CaseClasses.Intimacies.IntimacyStrength
import AtomExal.CaseClasses.Items.Item
import AtomExal.CaseClasses.{Experience, Merit, Specialty}
import AtomExal.{AbilityType, ExperienceChange}
import Charms.{CharmDB, CharmRef}

import scala.collection.mutable.ArrayBuffer
import scala.util.Random

/**
  * Created by Christopher on 17.07.2016.
  */
class RessourceManager {


    def newCharacter()(implicit characterImplicit : Character, charmDB : CharmDB) = {
      println("Setting a new character...")
      setDefaultCharacter()
    }



  def setDefaultCharacter()(implicit c : Character, charmDB : CharmDB) = {
    //clearing everything
    c.rxAbilityGroups.clear()
    c.rxSpecialties.clear()
    c.rxCharms.clear()

    c.setIntimacyTo("Cheesecake", Some(IntimacyStrength.Major))
    c.setIntimacyTo("Cheesecake", Some(IntimacyStrength.Defining))
    c.setIntimacyTo("Fighting the good fight", Some(IntimacyStrength.Major))
    c.setIntimacyTo("Playing Exalted", Some(IntimacyStrength.Major))
    c.setIntimacyTo("Dancing brings fun", Some(IntimacyStrength.Minor))
    c.setIntimacyTo("Cheesecake", None)
    c.rxSpecialties.+=(Specialty("Lore", "Lore", "Geology"))
    c.rxSpecialties.+=(Specialty("Melee", "Melee", "Swords"))
    c.rxSpecialties.+=(Specialty("Brawl", "Martial Arts", "Black Claw Style"))
    //assert(c.rxAbilityGroups.get.size == 0)
    //println(c.rxAbilityGroups.get)
    c.rxAbilityGroups.++=(RessourceManager.randomRatedAbilities(42))
    //assert(c.rxAbilityGroups.get.size == 5)
    //println(c.rxAbilityGroups.get)
    c.rxCharms.++=(RessourceManager.randomCharms(44))
    println(s"Number of Charms: ${c.charms.size}")


    c.rxMerits.+=(Merit(category = Merit.categories(0), name = "", rating = 3, id = Merit.newID()))
    c.rxMerits.+=(Merit(category = Merit.categories(0), name = "With name", rating = 4, id = Merit.newID()))
    c.rxMerits.+=(Merit(category = Merit.categories(1), name = "", rating = 2, id = Merit.newID()))
    c.rxMerits.+=(Merit(category = Merit.categories(2), name = "", rating = 1, id = Merit.newID()))


    c.rxItems.+=(Item(Item.newID(), isWeapon = true, title = "Longsword"))
    c.rxItems.+=(Item(Item.newID(), isWeapon = true, title = "Shortbow"))
    c.rxItems.+=(Item(Item.newID(), isArmor = true))
    c.rxItems.+=(Item(Item.newID(), title = "Amulet"))
    c.rxItems.+=(Item(Item.newID(), title = "Belt of Shadow Walking", commitmentCost = 5))


    Seq(ExperienceChange("From first session", Experience(Experience.GENERALXP, 5)), ExperienceChange("Spent for Sorcerous Working", Experience(Experience.GENERALXP, -4)), ExperienceChange("From second session", Experience(Experience.GENERALXP, 5))).foreach(d => if (c.canApplyChange(d)) c.applyChange(d))
  }
}

object RessourceManager {


  def defaultAbilities(ratingCalculator : () => Int = () => 0) : Seq[AbilityGroup] = {
    val ab = abilities.toSeq.map(a => {
      //each group
      val familiesT : Seq[AbilityFamily] = a._2.toSeq.map(b => {
        //each family
        val abilitiesT : Seq[SingleAbility]  = b._2._2.map(c => {
          //each ability
          SingleAbility(title = c, rating = ratingCalculator.apply())
        })

        AbilityFamily(fixed = b._2._1, abilities = abilitiesT, family = b._1)
      })

      AbilityGroup(group = a._1, families = familiesT, typ = AbilityType.Normal)
    })
    println(s"ab: $ab")
    ab
  }
  def randomRatedAbilities(seed : Int) : Seq[AbilityGroup] = {
    println("Building Random Abilities")
      val rand = new Random(seed)
      defaultAbilities(() => rand.nextInt(6))
  }

  def randomCharms(seed : Int)(implicit char : Character) : Seq[CharmRef] = {
    println("Charms")
    val rand = new Random(seed)
    val keys = CharmDB.default.map.keys.toSeq
    println(keys)
    val max = rand.nextInt(keys.size)
    val buffer: ArrayBuffer[CharmRef] = ArrayBuffer.empty[CharmRef]
    (1 to max).foreach(i => {
      println(i)
      val ind = rand.nextInt(keys.size)
      println(ind)
      if(!buffer.contains(keys(ind))) {
        println("in")
        buffer.+=(keys(ind))
      } else {
        println("out")
      }
    })
    println("buffer : " + buffer.toString())
    buffer.toSeq
  }



  val abilities: Map[String, (Map[String, (Boolean, Seq[String])])] = Map(
    "Archery" -> Map(
      "Archery" -> (true, Seq("Archery")),
      "Firearms" -> (true, Seq("Firearms"))
    ),
    "Craft" -> Map(
      "Craft" -> (false, Seq("Shipbuilding", "Blacksmithing", "Geomancy", "Artifact"))
    ),
    "Brawl/Martial Arts" -> Map(
      "Brawl" -> (true, Seq("Brawl")),
      "Martial Arts" -> (true, Seq("Martial Arts"))
    ),
    "Lore" -> Map(
      "Lore" -> (true, Seq("Lore"))
    ),
    "Melee" -> Map(
      "Melee" -> (true, Seq("Melee"))
    )
  )

  //require that all families are unique
  require({
    val raw = abilities.flatMap(_._2.keys).toSeq
    val dis = raw.distinct
    raw.size == dis.size
  })
}