package AtomImpl.Changes

/**
  * Created by Christopher on 17.07.2016.
  */
object Stacks {
  val Intimacy: Int = 10

  val Experience: Int = 9

  val Item: Int = 8

  val Willpower: Int = 5
  val Attribute : Int = 6
  val Strings: Int = 4
  val Merit : Int = 7
  val Specialty = 3

  val Costless = 0
  val Ability = 1
  val Charm = 2


  val values = Seq(Costless, Ability, Charm, Specialty, Strings, Willpower, Attribute, Merit, Item, Experience, Intimacy)
  assert(values.distinct.length == values.length)
  val max = values.max
  val min = values.min
}
