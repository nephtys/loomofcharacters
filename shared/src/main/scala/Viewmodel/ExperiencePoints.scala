package Viewmodel

import org.widok.{Buffer, Channel, ReadChannel, Var}

/**
  * Created by Christopher on 05.07.2016.
  */
class ExperiencePoints {

  val spentGeneral = Var[Int](0) // _3
  val spentSolar = Var[Int](0) // _4
  val spentSpecial = Var[Int](0) // _5

  val totalHistory : Buffer[(String, Long, Int, Int, Int)] = Buffer()

  //generatedvalues:
  totalHistory.changes.attach(delta => {
    totalGeneral := totalHistory.get.map(_._3).sum
    totalSolar := totalHistory.get.map(_._4).sum
    totalSpecial := totalHistory.get.map(_._5).sum
  })


  val totalGeneral : Var[Int] = Var[Int](0)
  val totalSolar : Var[Int] = Var[Int](0)
  val totalSpecial : Var[Int] = Var[Int](0)


}
