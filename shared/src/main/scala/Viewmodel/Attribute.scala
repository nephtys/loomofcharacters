package Viewmodel

import org.widok.Var

/**
  * Created by Christopher on 05.07.2016.
  */
class Attribute (val name : String, val category : String, val rating : Var[Int] = new Var[Int](0)) {

}