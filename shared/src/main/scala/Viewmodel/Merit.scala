package Viewmodel

import org.widok.Var

/**
  * Created by Christopher on 05.07.2016.
  */
class Merit(val category : String, val notes : Var[String] = Var(""), val rating : Var[Int] = Var(1)) {

}
