package Viewmodel

import Viewmodel.Intimacy.Minor
import org.widok.Var

/**
  * Created by Christopher on 05.07.2016.
  */
class Intimacy {
val strength : Var[Intimacy.EnumVal] = Var(Minor)
  val title : Var[String] = Var("")
}

object Intimacy {
  sealed abstract class EnumVal(val modifier : Int) {
    def >(o : Intimacy.EnumVal) = this.modifier > o.modifier
    def >=(o : Intimacy.EnumVal) = this.modifier >= o.modifier
    def <(o : Intimacy.EnumVal) = this.modifier < o.modifier
    def <=(o : Intimacy.EnumVal) = this.modifier <= o.modifier
    def ==(o : Intimacy.EnumVal) = this.modifier == o.modifier
  }
  case object Minor extends EnumVal(1)
  case object Major extends EnumVal(2)
  case object Defining extends EnumVal(3)
  val castes = Seq(Minor, Major, Defining)
}
