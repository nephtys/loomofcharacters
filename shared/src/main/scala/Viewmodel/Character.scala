package Viewmodel

import AtomExal.Caste
import org.widok.{Buffer, Dict, StateChannel, Var}

/**
  * Created by Christopher on 05.07.2016.
  */
class Character {
  val _name: Var[String] = Var("Klaus Klaussons")
  val _player: Var[String] = Var("Some player")
  val _casteStr: Var[Option[String]] = Var(Option.empty[String])

  val _castesPossibleStr : Buffer[String] = Buffer.from(Caste.castesString)


  val _concept: Var[String] = Var("a character designed to test this program")
  val _anima: Var[String] = Var("zeroes and ones all round him")

  //attributes
  val _attributes : Seq[Attribute] = Character.attributes.map(s => new Viewmodel.Attribute(s._1, s._2))

  val _attributesMap : Map[String, Attribute] = _attributes.map(b => (b.name, b)).toMap

  //abilities
  //TODO
  //val _abilityGroups: Seq[AbilityGroup] = ???

  val _abilityGroups : Var[Seq[AbilityGroup]] = Var(Character.abilitiesTransformedNull)

  val _abilityFamilies : Buffer[String] = Buffer("Lore", "Martial Arts", "Melee", "Brawl")


  //specialties
  val _specialties: Buffer[Specialty] = Buffer()

  //merits
  //val _selectedMeritCategory : Var[Option[String]] = Var(None)
  val _selectableMeritCategory = Buffer("Artifact", "Cult", "Backing", "Ressources", "Mutation: Wings")
  val _merits = Buffer(new Viewmodel.Merit(_selectableMeritCategory.apply(0)), new Viewmodel.Merit(_selectableMeritCategory.apply(2)), new Viewmodel.Merit(_selectableMeritCategory.apply(3)))

  //lazy val _merits: Buffer[Merit] = ???


  //willpower
  val _willpowerdots: Var[Int] = Var(0)


  //limit break and limit trigger
  val _limittrack: Var[Int] = Var(0)
  val _limittrigger: Var[String] = Var("")


  //experience points and bonus points
  val _experiencePoints = new ExperiencePoints()

  //items, weapons, armor
  //lazy val _items: Buffer[Item] = ???
  //lazy val _weapons: Buffer[Weapon] = ???
  //lazy val _armor: Buffer[Armor] = ???
  //TODO
  //TODO
  //TODO


  //intimacies
  val _intimacies: Buffer[Intimacy] = Buffer()

  //charms
  //TODO
  //lazy val _charms: Buffer[CharmRef] = ???
}


object Character {
  val attributes = Seq(("Strength", "Phyiscal") , ("Dexterity", "Phyiscal") , ("Stamina", "Phyiscal") , ("Charisma", "Social") , ("Manipulation", "Social") , ("Appearance", "Social") , ("Perception", "Mental") , ("Intelligence", "Mental") , ("Wits", "Mental") )

  val abilities: Map[String, (Map[String, (Seq[String])])] = Map(
    "Archery" -> Map(
      "Archery" -> Seq("Archery"),
      "Firearms" -> Seq("Firearms")
    ),
    "Craft" -> Map(
      "Craft" -> Seq()
    ),
    "Brawl/Martial Arts" -> Map(
      "Brawl" -> Seq("Brawl"),
      "Martial Arts" -> Seq("Martial Arts")
    ),
    "Lore" -> Map(
      "Lore" -> Seq("Lore")
    )
  )

  val abilitiesTransformedNull : Seq[AbilityGroup] = (
      abilities.toSeq.map(a => {
        //each group
        val families : Seq[AbilityFamily] = a._2.toSeq.map(b => {
          //each family
          val abilities : Buffer[Ability]  = Buffer.from(b._2.map(c => {
            //each ability
            new Ability {
                 title.:=(c)
                rating.:=(0)
            }
          }))

          new AbilityFamily(b._1, abilities)
        })

        new AbilityGroup(families)
      })
    )
}