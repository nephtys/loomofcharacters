package Traits

/**
  * Created by chris on 03.07.2016.
  */
case class HealthLevel(penalty : Option[Int], currentState : DamageType.EnumVal) {
assert(penalty.isEmpty || penalty.get == 0 || penalty.get == -1 || penalty.get == -2 || penalty.get == -4)
}
