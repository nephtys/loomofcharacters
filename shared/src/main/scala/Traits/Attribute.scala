package Traits

/**
  * Created by chris on 03.07.2016.
  */
case class Attribute (name : String, rating : Int) {
assert(rating >= 1)
}
