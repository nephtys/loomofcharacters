package Traits

import AtomExal.AbilityType

/**
  * Created by chris on 03.07.2016.
  */
case class AbilityFamily(name : String, ratedAbilities : Map[Ability, Int], bareminimum : Set[Ability], typ : AbilityType.EnumVal) { //some families have only one Ability (Lore, Melee), other have two (Brawl / Martial Arts or Archery / Firearms or Lore / Computers), other have unlimited Abilities (Craft)
//so every Ability has a minimum (Brawl + Martial Arts e.g., or Craft Shipbuilding, or Lore, or Melee
  //drawback: cannot model every Craft character


  assert(bareminimum.forall(a => ratedAbilities.contains(a)))

  assert(ratedAbilities.forall(a => a._2 >= 0))
}

object AbilityFamily {
  def brawl : AbilityFamily = ???
  def craft : AbilityFamily = ???
  def lore : AbilityFamily = ???
  def melee : AbilityFamily = ???
  def occult : AbilityFamily = ???
  def performance : AbilityFamily = ???

  def all = Set(brawl, craft, lore, melee, occult, performance)
}