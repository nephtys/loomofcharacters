package Traits

/**
  * Created by chris on 03.07.2016.
  */
case class HealthTrack(levels : Seq[HealthLevel]) {

}

object HealthTrack {
  def zero = HealthLevel(Some(0), DamageType.Undamaged)
  def one = HealthLevel(Some(1), DamageType.Undamaged)
  def two = HealthLevel(Some(2), DamageType.Undamaged)
  def four = HealthLevel(Some(4), DamageType.Undamaged)
  def inc = HealthLevel(None, DamageType.Undamaged)


  def default : HealthTrack = HealthTrack(Seq(zero, one, one, two, two, four, inc))
  assert(default.levels.size == 7)
}
