package Traits

/**
  * Created by chris on 03.07.2016.
  */
object DamageType {
  sealed trait EnumVal
  case object Undamaged extends EnumVal
  case object Bashing extends EnumVal
  case object Lethal extends EnumVal
  case object Aggravated extends EnumVal
  val castes = Seq(Undamaged, Bashing, Lethal, Aggravated)
}
