package Experience

/**
  * Created by chris on 03.07.2016.
  */
case class General(amount : Int) {
  def essence : Int = {
    if (amount < 50) {
      1
    } else if (amount < 125) {
      2
    } else if (amount < 200) {
      3
    } else if (amount < 300) {
      4
    } else {
      5
    }
  }
}
