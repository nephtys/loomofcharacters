package Controls

import Model.ChangeImplementation.ZeroChange
import Model.{CharmDatabase, CombinedChange, ModelCharacter}
import org.widok.{Buffer, Channel, DOM, ReadChannel, StateChannel, Var, View}
import org.widok.bindings.HTML.Label
import org.widok.bindings.Bootstrap.Glyphicon
import org.widok.bindings.HTML.Label
import Model.{CharmDatabase, CombinedChange, ModelCharacter}
import org.scalajs.dom.html._
import org.widok.html._

import scala.Option
import scala.concurrent.Future
import scala.concurrent.Future
import scala.scalajs.concurrent.JSExecutionContext.Implicits.queue

/**
  * Created by chris on 11.07.2016.
  */
class ComboSelector(val lbl : StateChannel[String] = new Var(""),
                    val value : StateChannel[Option[String]] = new Var(None),
                    val values : Buffer[String] = Buffer[String](),
                    val default : String = "",
val generateChange: StateChannel[(String) => CombinedChange] = Var(t => ZeroChange),
val selectorModal: StateChannel[(Seq[CombinedChange]) => Future[Option[CombinedChange]]] = Var( s => Future {s.headOption}))
  (implicit val char : StateChannel[ModelCharacter], charmdb : StateChannel[CharmDatabase], vm : StateChannel[Viewmodel.Character])   extends Label with Selectorable[String] {


  private val vinnerdefault = None
  private val vinnerbefore : Var[Option[String]] = Var(vinnerdefault)
  private val vinner : Var[Option[String]] = Var(vinnerdefault)
  val sel =  select().default(default).bind(values, option, vinner)

  override val rendered: Element = DOM.createElement("div", Seq.apply[View](
    label(lbl.get), select().default("").bind(values, option, vinner)
  ))

  /*
  println("Testing setter")
  vinner.:=(Some(values.get.head))
  println("setter tested")

  println("Testing getter")
  println(vinnerbefore.get)
  println("getter tested")

  vinner.:=(vinnerbefore.get)
*/

  //css("eqi-container")

  value.attach(t => {
    vinnerbefore.:=(t)
    vinner.:=(t)
  })

  vinner.attach((t : Option[String]) => {
    println("someting attaching Zero")
      if (vinnerbefore.get != t) {
        //create change stuff
        println("someting attaching A")
        val v = vinnerbefore.get

        println("someting attaching B" )
        t.foreach(s => executeSelection(s))
        println("someting attaching C")

        vinner.:=(v)
        println("someting attaching D")
        vinnerbefore.:=(v)

        //while not chosen set value to previous immediately


      } else {
        println("not something")
      }
  })

  override protected def executeSelection(t: String): Unit = {
    println("executing from comboselector")
    onSelection(t,
      generateChange.get,
      selectorModal.get)
  }
}
