package Controls

import AtomExal.{AtomicExchange, NameChange}
import Charms.CharmDB
import Model.ChangeImplementation.ZeroChange
import Model.{CharmDatabase, CombinedChange, ModelCharacter}
import org.scalajs.dom.html._
import org.widok.bindings.Bootstrap.Glyphicon
import org.widok.bindings.HTML.Label
import org.widok.{DOM, StateChannel, Var}

import scala.Option
import scala.concurrent.Future
import scala.concurrent.Future
import scala.scalajs.concurrent.JSExecutionContext.Implicits.queue
import scala.concurrent.Future

/**
  * Created by Christopher on 18.07.2016.
  */
class IntervalSelectorAtom (val largestValue : StateChannel[Int] = Var(5), val  minimalValue : StateChannel[Int] = Var(0),
                            val currentValue : StateChannel[Int] = Var(3),
                            val generateChange: (Int) => Seq[AtomicExchange] = t => Seq(NameChange("old", "new")),
                            val selectorModal: (Set[Seq[AtomicExchange]]) => Future[Option[Seq[AtomicExchange]]] = s => Future {
                              s.headOption
                            })
                           (implicit val char : AtomImpl.Character, charmDB : CharmDB)  extends Label with SelectorableAtom[Int]{

  val selected : Seq[Var[Boolean]] = (1 to largestValue.get).map(i => Var( minimalValue.get > i ))


  private val circles = (1 to largestValue.get).flatMap(i => Seq(icon(i, select = true), icon(i, select = false)))

  private def icon(i : Int, select : Boolean) = {
    def binder = selected( i - 1 )
    (if (select) Glyphicon.Check() else Glyphicon.Unchecked()).show(binder.isNot(!select)).onClick(e => clicked(i))
  }

  private def clicked(i : Int) = {
    println(s"Pressed button $i")
    val already = selected(i - 1).get && (i >= largestValue.get || !selected(i).get)
    setTo(if (already) {
      i - 1
    } else {
      i
    })
  }

  currentValue.attach(newval => {
    if (newval <= largestValue.get && newval >= 0) {
      (1 to largestValue.get).foreach(a => selected(a - 1).:=(newval >= a))
    } else {
      println("ERROR IN INTERVALPOINTS! OUTSIDE INTERVAL")
    }
  })

  private def setTo(newval : Int) = {
    println(s"Setting to $newval")
    if (newval <= largestValue.get && newval >= 0) {
      executeSelection(newval)
    }
  }

  override val rendered: Element = DOM.createElement("div", circles)
  css("eqi-container")


  override protected def executeSelection(t : Int): Unit = {
    onSelection(t,
      generateChange,
      selectorModal)
  }

}