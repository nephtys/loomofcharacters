package Controls

import Model.{CharmDatabase, CombinedChange, ModelCharacter}
import org.widok.StateChannel

import scala.concurrent.Future
import scala.scalajs.concurrent.JSExecutionContext.Implicits.queue
/**
  * Created by chris on 11.07.2016.
  */
trait Selectorable[T] {

  def selectorItself = this


  protected def executeSelection(t: T): Unit

  def generateChange: StateChannel[((T => CombinedChange))]

  def selectorModal: StateChannel[((Seq[CombinedChange]) => Future[Option[CombinedChange]])]

  protected def onSelection(
                             newValue: T,
                             genChange: (T => CombinedChange),
                             selectorModal: (Seq[CombinedChange]) => Future[Option[CombinedChange]])
                           (implicit char : StateChannel[ModelCharacter],
  charmdb: StateChannel[CharmDatabase], vm: StateChannel[Viewmodel.Character]) : Unit = {
    implicit val charI = char.get
    implicit val cdbI = charmdb.get
    implicit val vmI = vm.get

    println("creating future")
    val changeOpt: Future[Option[CombinedChange]] = selectorModal(genChange.apply(newValue).calculateCosts())
    changeOpt.map(s => {
      println(s)
      s
    }).filter(_.isDefined).map(_.get).foreach(c => if (c.isAllowed()) {
      println("Writing from Selector")
      c.writeToVM()
      c.writeToModel()
    } else {
      println("Not allowed anymore")
    })
  }
}
