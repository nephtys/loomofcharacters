package Controls

import org.scalajs.dom.html.Element
import org.widok.bindings.Bootstrap.{Modal, Panel}
import org.widok.bindings.HTML.{Button, Raw}
import org.widok.{DOM, Var, View, Widget, bindings}
import org.widok.bindings.Bootstrap.{Glyphicon, Panel}
import org.widok.bindings.HTML.Label
import org.widok.html._

/**
  * Created by chris on 08.07.2016.
  */
class CloseableModal(content: View, header : String = "", myId : String = "myModalId" ) extends Widget[CloseableModal] {

  //TODO: crashes until now during runtime!


  private val modalBodyDiv : bindings.HTML.Container.Generic = div(content).css("modal-body")
  private val modalHeaderDiv : bindings.HTML.Container.Generic = div(Button("&times;").css("close").attribute("data-dismiss", "modal"), h4(header)).css("modal-header")
  private val modalFooterDiv : bindings.HTML.Container.Generic = div(Button("Close").css("btn btn-default").attribute("data-dismiss", "modal")).css("modal-footer")
  private val modalContentDiv : bindings.HTML.Container.Generic = div(modalHeaderDiv, modalBodyDiv, modalFooterDiv).css("modal-content")
  private val modalDialogDiv : bindings.HTML.Container.Generic = div(modalContentDiv).css("modal-dialog")
  private val modalContainerDiv : bindings.HTML.Container.Generic = div(modalDialogDiv).css("modal", "fade").attribute("role", "dialog")

  override val rendered: Element = DOM.createElement("div", Seq(modalBodyDiv))


  def getButtonToOpen = Raw(s"""<button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#$myId">Open Modal</button>""")
}
