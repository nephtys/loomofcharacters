package Controls

import AtomExal.AbilityType
import Model.ChangeImplementation._
import Model._
import org.widok.bindings.Bootstrap.{Button, Glyphicon}
import org.widok.bindings.HTML.Label
import Model.{CharmDatabase, CombinedChange, ModelCharacter}
import org.scalajs.dom.html._
import org.widok.html._

import scala.Option
import scala.concurrent.Future
import scala.concurrent.Future
import scala.scalajs.concurrent.JSExecutionContext.Implicits.queue
import Model.{CharmDatabase, CombinedChange, ModelCharacter}
import Viewmodel.{Ability, AbilityGroup}
import org.scalajs.dom.html._
import org.widok.{Buffer, DOM, Length, StateChannel, Var, View}
import org.widok.bindings.HTML.Label

import scala.{Option, scalajs}
import scala.concurrent.Future
import scala.concurrent.Future
import scala.scalajs.concurrent.JSExecutionContext.Implicits.queue
import scala.concurrent.Future
import scala.scalajs.js

/**
  * Created by Christopher on 14.07.2016.
  */

class AbilityMultiControl(val group : StateChannel[Seq[AbilityGroup]] = Var(Seq.empty[AbilityGroup])
                         ,val selectorModal: StateChannel[(Seq[CombinedChange]) => Future[Option[CombinedChange]]] = Var( s => Future {s.headOption}))
                         (implicit val char : StateChannel[ModelCharacter], charmdb : StateChannel[CharmDatabase], vm : StateChannel[Viewmodel.Character]) extends Label
{
  private implicit val cdbi = charmdb.get
  private implicit val chari = char.get
  private implicit val vmi = vm.get

  private def comp(x : CombinedChange) = {
    val changeOpt: Future[Option[CombinedChange]] = selectorModal.get.apply(x.calculateCosts())
    println("Future is done")
    changeOpt.filter(_.isDefined).map(_.get).foreach(c => if (c.isAllowed()) {
      println("Writing to VM and Model now")
      c.writeToVM()
      c.writeToModel()
    })
  }


  override val rendered: Element = DOM.createElement("div", Seq.apply[View](
    ul(
      Buffer.from[org.widok.Widget.List.Item[_]](
        group.get.map(gr => {
          li(new ComboSelector(lbl = Var("Typ"), value = gr.typ,generateChange = Var((i : String) => SetAbilityType(gr, AbilityType.stringToCaste(Some(i)))), values = Buffer.from[String](AbilityType.typesS)) {
            //set ability type

          }.width(Length.Pixel(40)), ul(
            Buffer.from(gr.families.map(fam => {
              val textfield = Var("")
              li(div(label(fam.name), text().width(new Length.Pixel(70)).bind(textfield).placeholder("new ability"), Button(Glyphicon.Plus).onClick(e => {
                if (textfield.get.nonEmpty) {
                  //adding a new ability to the family
                  val x = AddNewAbility(fam, textfield.get)
                  textfield.:=("") //clear field
                  comp(x)
                }
              })), ul(
                fam.abilities.map( (ab : Ability) => li(text().subscribe(ab.title).enabled(false), Button(Glyphicon.Remove).onClick(e => {
                  //removing a ability
                  if (ab.rating.get == 0) {
                    val x = RemoveAbility(fam, ab)
                    comp(x)
                  }
                }), new IntervalSelector(currentValue = ab.rating,generateChange = Var(((i : Int) => {
                  println(s"generating change to $i")

                  SetAbilityRating(gr.typ.get.isDefined && gr.typ.get.get != AbilityType.Normal.toString, ab, i)
                })), selectorModal = this.selectorModal) {
                  //set ability rating
                }))
              )
              )
            })
          )))
        })
      )
    )

  ))

  //Do not forget that type can only be changed during Chargen, and you can not decrease rating during play


}
