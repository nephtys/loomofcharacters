package Controls

import Model.ChangeImplementation.ZeroChange
import Model.{CharmDatabase, CombinedChange, ModelCharacter}
import org.widok.{StateChannel, Var}
import org.widok.bindings.HTML.Label
import org.widok.html._

import scala.concurrent.Future
import scala.scalajs.concurrent.JSExecutionContext.Implicits.queue
import scala.scalajs.js.timers._


/**
  * Created by chris on 11.07.2016.
  */
class TextSelector(
                    val value : StateChannel[String] = Var("")
                    , val categoryLabel : Option[String] = None
                    , val promptText : Option[String] = None
                  ,val generateChange: StateChannel[(String) => CombinedChange] = Var(t => ZeroChange)
                    ,val selectorModal: StateChannel[(Seq[CombinedChange]) => Future[Option[CombinedChange]]] = Var( s => Future {s.headOption}))
                  (implicit val char : StateChannel[ModelCharacter], charmdb : StateChannel[CharmDatabase], vm : StateChannel[Viewmodel.Character])  extends Label with Selectorable[String] {
//shows a bold category label next to the TextField, with a padding between them

//if the text is changed, execute given code after a delay
  val textValue : Var[String] = Var("")
  textValue.subscribe(value)

var typingTimer : Option[SetTimeoutHandle] = None
val doneTypingInterval = 2000


  def checkDifferent : Boolean = textValue.get == value.get
  val di = div(label(categoryLabel.get).attribute("style", "margin-right: 15px;"), text().bind(textValue).placeholder(promptText.get).onKeyUp(f => {
    typingTimer.foreach(clearTimeout)
    typingTimer = Some(setTimeout(doneTypingInterval) {
      if (checkDifferent) {
        executeSelection(textValue.get)
      }
    })
  }).onKeyDown(f => typingTimer.foreach(clearTimeout)) ).attribute("style", "display: inline-block;")

  override protected def executeSelection(t: String): Unit = {
    onSelection(t,
      generateChange.get,
      selectorModal.get)
  }
}
