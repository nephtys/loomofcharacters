package Controls

import org.scalajs.dom.html.{Div, Element}
import org.widok.bindings.Bootstrap.{Glyphicon, Panel}
import org.widok.bindings.HTML.Label
import org.widok.{Channel, DOM, Var, View, Widget}
import org.widok.html._

/**
  * Created by Christopher on 05.07.2016.
  */
class IntervalPoints(largestValue : Int = 5, minimalValue : Int = 0)  extends Label{
val currentValue : Var[Int] = Var(minimalValue)
  val selected : Seq[Var[Boolean]] = (1 to largestValue).map(i => Var( minimalValue > i ))


  private val circles = (1 to largestValue).flatMap(i => Seq(icon(i, select = true), icon(i, select = false)))

  private def icon(i : Int, select : Boolean) = {
    def binder = selected( i - 1 )
    (if (select) Glyphicon.Check() else Glyphicon.Unchecked()).show(binder.isNot(!select)).onClick(e => clicked(i))
  }

  private def clicked(i : Int) = {
    println(s"Pressed button $i")
    val already = selected(i - 1).get && (i >= largestValue || !selected(i).get)
    setTo(if (already) {
      i - 1
    } else {
      i
    })
  }

  currentValue.attach(newval => {
    if (newval <= largestValue && newval >= 0) {
      (1 to largestValue).foreach(a => selected(a - 1).:=(newval >= a))
    } else {
      println("ERROR IN INTERVALPOINTS! OUTSIDE INTERVAL")
    }
  })

  private def setTo(newval : Int) = {
    println(s"Setting to $newval")
    if (newval <= largestValue && newval >= 0) {
      currentValue.:=(newval)
    }
  }

  override val rendered: Element = DOM.createElement("div", circles)
    css("eqi-container")


  def bind(c : Channel[Int]) : IntervalPoints = {
    this.currentValue.bind(c)
    this
  }
}
