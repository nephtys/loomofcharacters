package Controls

import AtomExal._
import AtomExal.CaseClasses.Abilities.{AbilityGroup, SingleAbility}
import AtomExal.CaseClasses.Specialty
import AtomImpl.Changes._
import Charms.CharmDB
import Model.ChangeImplementation.AddNewAbility
import Model.CharmDatabase
import org.scalajs.dom.html._
import org.widok.bindings.Bootstrap.{Button, Glyphicon}
import org.widok.bindings.HTML.Label
import org.widok.html._
import org.widok.{Buffer, DOM, Length, StateChannel, Var, View, bindings}
import org.widok.bindings.HTML.Label
import org.widok.bindings.HTML.Label
import Model.{CharmDatabase, CombinedChange, ModelCharacter}
import org.scalajs.dom.html._
import org.widok.html._

import scala.{Option, scalajs}
import scala.concurrent.Future
import scala.concurrent.Future
import scala.scalajs.concurrent.JSExecutionContext.Implicits.queue
import scala.concurrent.Future
import scala.scalajs.js
import scala.concurrent.Future
import scala.scalajs.js.timers._
import org.widok.bindings.Bootstrap.{Button, Checkbox, Glyphicon}
import org.widok.html._

import scala.concurrent.Future
import scala.concurrent.Future
import scala.scalajs.concurrent.JSExecutionContext.Implicits.queue
import scala.concurrent.Future
import scala.scalajs.js
import scala.concurrent.Future
import scala.concurrent.Future
import scala.scalajs.js.timers._
import scala.concurrent.Future
import scala.scalajs.concurrent.JSExecutionContext.Implicits.queue
import scala.scalajs.js.timers._

/**
  * Created by Christopher on 18.07.2016.
  */
class AbilityMultiControlAtom (val groups : Buffer[AbilityGroup],
                               val specialties : Buffer[Specialty])
                              (implicit val char : AtomImpl.Character, charmDB : CharmDB, selectorModal: (Set[Seq[AtomicExchange]]) => Future[Option[Seq[AtomicExchange]]]) extends Label
{

  private def comp(x : Seq[AtomicExchange]) = {
    val changeOpt: Future[Option[Seq[AtomicExchange]]] = selectorModal.apply(char.findChangePaths(x))
    println("Future is done")
    changeOpt.map(s => {
      println(s)
      s
    }).filter(_.isDefined).map(_.get).foreach(c => if (c.forall(p => char.canApplyChange(p))) {
      println("Writing from Selector")
      c.foreach(e => char.applyChange(e))
    } else {
      println("Not allowed anymore")
    })
  }

  //println(s"Building AbilityMultiControlAtom with Buffer : $groups")

  //TODO: use AbilityFamily.fixed for layouting, also optimize with more statically assigned values

  private val types =  Buffer.from(AbilityType.typesS)

  override val rendered: Element = DOM.createElement("div", Seq.apply[View](
    ul(
          groups.map(gr => {
            println(s"Doing AbilityGroup ${gr.group}")
            li(
              {

                var typingTimer : Option[SetTimeoutHandle] = None


                val str = AtomExal.AbilityType.casteToString(Some(gr.typ))
                //TODO: still creating a cycle
                val x: Var[Option[String]] = Var(str)
                val resetTimer = 2000
                x.attach(so => {
                  println(s"changed to $so")
                  if (str != so) {
                    typingTimer.foreach(clearTimeout)
                    typingTimer = Some(setTimeout(resetTimer) {
                      println("Timer called")
                      if (x.get != str) {
                        println(s"setted from Timer from ${x.get} to $str")
                        x.:=(str)
                      }
                    })

                    val t = AbilityType.stringToCaste(so)
                    val change = Seq(AbilityTypeChange(group = gr.group, from = gr.typ, to = t.getOrElse(AbilityType.Normal)))
                    comp(change)
                  }
                })
                val sel: bindings.HTML.Input.Select = select().bind(types, option, x).enabled(char.rxCharGenFinished.isNot(true))

                sel
              }
              //new ComboSelector(lbl = Var("Typ"), value = gr.typ,generateChange = Var((i : String) => AbilityTypeChange(group : String, from : Caste.EnumVal,
              //  to = AbilityType.stringToCaste(Some(i))) AbilityTypeChange(gr, )), values = Buffer.from[String](AbilityType.typesS)) {
              //set ability type

              //}.width(Length.Pixel(40))
              , ul(
                Buffer.from(gr.families.map(fam => {
                  val abilitytextfield = Var("")

                  val specialtytextfield = Var("")
                  val specialtyList = ul(
                    specialties.filter(t => t.group == gr.group && t.family == fam.family).map(s => li(s.title))
                  )
                  val specialtyLbl = label("Add Specialty:")
                  val specialtyAddBtn = Button(Glyphicon.Plus).onClick(e => {
                    if (specialtytextfield.get.nonEmpty) {
                      //adding a new ability to the family
                      val x = SpecialtyAddChange(group = gr.group, family = fam.family, specialty = specialtytextfield.get)
                      specialtytextfield.:=("") //clear field
                      comp(Seq(x))
                    }
                  })
                  val specialtyTextField = text().width(new Length.Pixel(70)).bind(specialtytextfield).placeholder("new specialty")

                  val specialybox = div(specialtyLbl, specialtyTextField, specialtyAddBtn)

                  li(div(label(fam.family), text().width(new Length.Pixel(70)).bind(abilitytextfield).placeholder("new ability").visible(!fam.fixed), Button(Glyphicon.Plus).onClick(e => {
                    if (abilitytextfield.get.nonEmpty) {
                      //adding a new ability to the family
                      val x = AbilityAddChange(group = gr.group, family = fam.family, newAbility = abilitytextfield.get)
                      abilitytextfield.:=("") //clear field
                      comp(Seq(x))
                    }
                  }).visible(!fam.fixed)), ul(
                    Buffer.from(fam.abilities.map((ab: SingleAbility) => {
                      val interval = new IntervalSelectorAtom(currentValue = Var(ab.rating), generateChange = (i: Int) => {
                        println(s"generating change to $i")
                        if (i > ab.rating) {
                          //add
                          println(s"adding to ability ${ab.title}")
                          val distance = i - ab.rating
                          (1 to distance).map(k => AbilityRatingChange.apply(group = gr.group, family = fam.family, ability = ab.title, from = ab.rating + (k - 1), to = ab.rating + k))
                        } else if (i < ab.rating) {
                          //subtract
                          println(s"subtracting from ability ${ab.title}")
                          val distance = ab.rating - i
                          (1 to distance).map(k => AbilityRatingChange.apply(group = gr.group, family = fam.family, ability = ab.title, from = ab.rating - (k - 1), to = ab.rating - k))
                        } else {
                          Seq.empty
                        }
                      }, selectorModal = this.selectorModal) {
                        //set ability rating
                      }
                      val removeBtn = Button(Glyphicon.Remove).onClick(e => {
                        //removing a ability
                        if (ab.rating == 0) {
                          val x = AbilityRemoveChange(group = gr.group, family = fam.family, abilityToDelete = ab.title)
                          comp(Seq(x))
                        }
                      })


                      if (fam.fixed) {
                        li(label(ab.title), interval)
                      } else {
                        li(label(ab.title), removeBtn, interval)
                      }})
                    )
                  )
                    ,label("Specialties:").css("bold-text")
                    ,specialtyList
                    ,specialybox

                  )
                })
                )))
          }
      )
    )

  ))



}
