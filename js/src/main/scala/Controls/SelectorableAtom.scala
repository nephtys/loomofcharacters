package Controls

import AtomExal.AtomicExchange
import Charms.CharmDB
import Model.{CharmDatabase, CombinedChange, ModelCharacter}
import org.widok.StateChannel

import scala.concurrent.Future
import scala.scalajs.concurrent.JSExecutionContext.Implicits.queue
import scala.concurrent.Future

/**
  * Created by Christopher on 18.07.2016.
  */
trait SelectorableAtom[T] {

  def selectorItself = this


  protected def executeSelection(t: T): Unit

  def generateChange :  (Int) => Seq[AtomicExchange]

  def selectorModal: (Set[Seq[AtomicExchange]]) => Future[Option[Seq[AtomicExchange]]]

  protected def onSelection(
                             newValue: T,
                             genChange: (T => Seq[AtomicExchange]),
                             selectorModal: (Set[Seq[AtomicExchange]]) => Future[Option[Seq[AtomicExchange]]])
                           (implicit char : AtomImpl.Character, charmDB: CharmDB) : Unit = {

    println("creating future")
    val paths : Set[Seq[AtomicExchange]] = char.findChangePaths(genChange.apply(newValue))
    val changeOpt: Future[Option[Seq[AtomicExchange]]] = selectorModal(paths)
    changeOpt.map(s => {
      println(s)
      s
    }).filter(_.isDefined).map(_.get).foreach(c => if (c.forall(p => char.canApplyChange(p))) {
      println("Writing from Selector")
      c.foreach(e => char.applyChange(e))
    } else {
      println("Not allowed anymore")
    })
  }
}
