package example

import org.widok._
/**
  * Created by chris on 03.07.2016.
  */
object Routes {
  val character     = Route("/"           , pages.CharacterSheetPage    )
  val main     = Route("/main"           , pages.Main    )
  val test     = Route("/test/:param", pages.Test    )
  val notFound = Route("/404"        , pages.NotFound)

  val routes = Set(character, main, test, notFound)
}
