package example.helper

import java.util.concurrent.atomic.AtomicInteger

import AtomExal._
import AtomExal.CaseClasses.Intimacies.IntimacyStrength
import AtomExal.CaseClasses.{Experience, Merit}
import AtomExal.Caste.CasteTyp
import AtomImpl.Changes._
import AtomImpl.{Character, RessourceManager}
import Charms.{CharmDB, CharmRef}
import Controls.{AbilityMultiControlAtom, IntervalSelectorAtom}
import org.scalajs.dom.ext.KeyCode
import org.widok.bindings.Bootstrap.{Button, Checkbox, Glyphicon}
import org.widok.html._
import org.widok.{Buffer, Channel, Length, ReadBuffer, Var, Widget, bindings}

import scala.concurrent.Future
import scala.scalajs.concurrent.JSExecutionContext.Implicits.queue
import scala.scalajs.js.timers._

/**
  * Created by Christopher on 18.07.2016.
  */
object CharacterSubViews {

  val i = new AtomicInteger(0)

  def accordion[T](header : String, body : Widget[T], level : Int = 2) : bindings.HTML.Container.Generic = {
    assert(level == 2 || level == 4)
    val bid = "elementID"+i.getAndIncrement().toString
      div(Button(if(level == 2) {h2(header)} else {h4(header)}).attribute("data-toggle", "collapse").attribute("data-target", "#" + bid), div(body).css("collapse").id(bid))
  }


  def controlBar(implicit manager: RessourceManager, char: Character, charmDB : CharmDB): bindings.HTML.Container.Generic = {
    div(div(h4("ControlBar")), div(Button("Make A Fresh Character").onClick(f => manager.newCharacter())))
  }

  def characterableView(implicit char: Character, charmdb: CharmDB, selectorModal: (Set[Seq[AtomicExchange]]) => Future[Option[Seq[AtomicExchange]]]): bindings.HTML.Container.Generic = {


    def lbldBtn(lbl : String = "somelabel", source : Var[String] = Var("content"), doWhenClicked  : String => Option[AtomicExchange] = (s : String) => None, resetTimer : Int = 5000) = {
      val v = Var("")
      var typingTimer : Option[SetTimeoutHandle] = None

      def store() = {
        typingTimer.foreach(clearTimeout)
        typingTimer = None
        val o = doWhenClicked.apply(v.get)
        o.foreach( c => if (char.canApplyChange(c)) char.applyChange(c))
      }

      div(label(lbl), text().bind(v).subscribe(source).onKeyUp(f => {
        //reset after a while
        typingTimer.foreach(clearTimeout)
        typingTimer = Some(setTimeout(resetTimer) {
          if (v.get != source.get) {
            v.:=(source.get)
          }
        })
      }).onKeyPress(f => if (f.keyCode == KeyCode.Enter) store()), Button(Glyphicon.Save()).onClick(f => store())).css("div-without-linebreak")
    }

    def comp(x : Seq[AtomicExchange])(implicit charmdb : CharmDB, char : Character) = {
      val changeOpt: Future[Option[Seq[AtomicExchange]]] = selectorModal.apply(char.findChangePaths(x))
      println("Future is done")
      changeOpt.map(s => {
        println(s)
        s
      }).filter(_.isDefined).map(_.get).foreach(c => if (c.forall(p => char.canApplyChange(p))) {
        println("Writing from Selector")
        c.foreach(e => char.applyChange(e))
      } else {
        println("Not allowed anymore")
      })
    }

    def casteSelector( source : Var[Option[CasteTyp]], resetTimer : Int = 5000) = {
      var typingTimer : Option[SetTimeoutHandle] = None
      //TODO: still creating a cycle
      val x: Var[Option[String]] = Var(Caste.casteToString(source.get))
      source.attach(p => x.:=(Caste.casteToString(p)))
      val types = Buffer.from(Caste.castesString)
      x.attach(so => {
        println(s"changed to $so")
        if (Caste.casteToString(source.get) != so) {
          typingTimer.foreach(clearTimeout)
          typingTimer = Some(setTimeout(resetTimer) {
            println("Timer called")
            if (x.get != Caste.casteToString(source.get)) {
              println(s"resetted from Timer from ${x.get} to ${Caste.casteToString(source.get)}!")
              x.:=(Caste.casteToString(source.get))
            }
          })

          val oldC = source.get
            val newC = Caste.stringToCaste(so)
          val change = Seq(CasteChange(from = oldC, to = newC))
          comp(change)
        }
      })
      val sel: bindings.HTML.Input.Select = select().bind(types, option, x).enabled(char.rxCharGenFinished.isNot(true))

      div(label("Caste"), sel ).css("div-without-linebreak")
    }

    accordion("Character View", div(
      lbldBtn("name", char.rxName, s => if (s.nonEmpty && s != char.name) Some(NameChange(char.name, s)) else None),
      lbldBtn("player", char.rxPlayer, s => if (s.nonEmpty && s != char.player) Some(PlayerChange(char.player, s)) else None),
      casteSelector(char.rxCaste  ),
      lbldBtn("concept", char.rxConcept, s => if (s.nonEmpty && s != char.concept) Some(ConceptChange(char.concept, s)) else None),
      lbldBtn("anima", char.rxAnima, s => if (s.nonEmpty && s != char.anima) Some(AnimaChange(char.anima, s)) else None)

    ).css("flex-container"), 2)
  }

  def Choose(v : Channel[String], options : Buffer[String], default : String = "")(name : String, wi : Int = 50)  = {
    div(
      label(name), select().bind(options, option, v.biMap[Option[String]](s => Some(s), o => o.getOrElse(default)))
    ).width(Length.Pixel(wi))
  }
  //def Choose(v : Channel[Int])(name : String, wi : Int = 50) = Choose(v.biMap(i => i.toString, s => Integer.parseInt(s)))(name, wi)

  def experiencableView(implicit char: Character, charmdb: CharmDB): bindings.HTML.Container.Generic = {

    val reasonGain = Var("")
    val reasonSpending = Var("")
    val categorySpending = Var(Experience.GENERALXP)
    val categoryGain = Var(Experience.GENERALXP)
    val numberGain = Var(0)
    val numberSpending = Var(0)

    val categories = Buffer.from(Experience.types)

    div(accordion("Experience Points and CharGen", div(
      Checkbox(
        checkbox().bind(char.rxBonusPointsOn)
        , "Use Bonus Points"
      ),
      Checkbox(
        checkbox().subscribe(char.rxCharGenFinished.isNot(true)).enabled(false)
        , "Currently in CharGen"
      ),
      Button("Finish CharGen").onClick(f => {
        char.setCharGenFinished(true)
      }).show(char.rxCharGenFinished.is(false))
      , h4("Experiences Overview")
      , ul(char.rxExperience.filter(p => !p.same || AtomExal.CaseClasses.Experience.showIfFull(p.cat)).map(p => {
          li(AtomExal.CaseClasses.Experience.toString(p.cat) + " : " + p.string)
      }))
      , accordion(("Gain Experience Points manually"), div(Text(reasonGain)("Reason for gaining XP", 120),
        Choose(categoryGain.biMap(i => Experience.toString(i), s => Experience.fromString(s)), categories, Experience.toString(Experience.GENERALXP))("Category", 120),
        Number(numberGain)("Number of XP"),
        Button(Glyphicon.PlusSign()).enabled(reasonGain.map(f => !f.isEmpty))
          .onClick(f => {
            val c = ExperienceChange(reasonGain.get, Experience(cat = categoryGain.get, amount = numberGain.get ))
            if(char.canApplyChange(c) && numberGain.get > 0) {
              reasonGain.:=("")
              numberGain.:=(0)
              char.applyChange(c)
            }
          })
      ),4)

      , accordion("Spent Experience Points manually"
      , div(Text(reasonSpending)("Reason for spending XP", 120),
        Choose(categorySpending.biMap(i => Experience.toString(i), s => Experience.fromString(s)), categories, Experience.toString(Experience.GENERALXP))("Category", 120),
        Number(numberSpending)("Number of XP"),
        Button(Glyphicon.MinusSign()).enabled(reasonSpending.map(f => !f.isEmpty))
              .onClick(f => {
                val c = ExperienceChange(reasonSpending.get, Experience(cat = categorySpending.get, amount = (-1) * numberSpending.get ))
                if(char.canApplyChange(c) && numberSpending.get > 0) {
                  reasonSpending.:=("")
                  numberSpending.:=(0)
                  char.applyChange(c)
                }
              })
      ), 4)
      , accordion("Experience history"
      , ul(char.rxExpChanges.map( change => {
        li (s"${Experience.toString(change.increment.cat)} ${if(change.increment.amount > 0) "+" else ""}${change.increment.amount}      ---      ${(change.reason)}")
      })), 4).visible(char.rxExpChanges.nonEmpty)
    ), 2))
  }

  def attributableView(implicit char: Character, charmdb: CharmDB, thisselectorModal: (Set[Seq[AtomicExchange]]) => Future[Option[Seq[AtomicExchange]]]): bindings.HTML.Container.Generic = {
    def interval(rating : Int, title : String) = new IntervalSelectorAtom(currentValue = Var(rating), generateChange = (i: Int) => {
      println(s"generating change to $i")
      if (i > rating) {
        //add
        val distance = i - rating
        (1 to distance).map(k => AttributeIncreaseByOneChange(attribute = title, i = +1))
      } else if (i < rating) {
        //subtract
        val distance = rating - i
        (1 to distance).map(k => AttributeIncreaseByOneChange(attribute = title, i = -1))
      } else {
        Seq.empty
      }
    }, selectorModal = thisselectorModal)

    div(accordion("Attributes"
      ,div(
        char.rxAttributes.map(a => div(label(a.name), interval(a.rating, a.name)).css("attribute-element"))
      ).css("attribute-container")
    ,2))
  }

  def abilitableView(implicit char: Character, charmdb: CharmDB, selectorModal: (Set[Seq[AtomicExchange]]) => Future[Option[Seq[AtomicExchange]]]): bindings.HTML.Container.Generic = {
    div(accordion("Abilities and Specialties"
      , new AbilityMultiControlAtom(char.rxAbilityGroups, char.rxSpecialties), 2
    ))
  }

  def intimacyView(implicit char: Character, charmdb: CharmDB, selectorModal: (Set[Seq[AtomicExchange]]) => Future[Option[Seq[AtomicExchange]]]): bindings.HTML.Container.Generic = {
    val tf = Var("")
    val is :Var[Option[String]] = Var(Some(IntimacyStrength.Minor.toString))

    div(div(accordion("Intimacies",div(
      ul(char.rxIntimacies.map(intimacy => {
        li(label(intimacy.level.toString),label("|||"), label(intimacy.title))
      })),
      div(label("Add or change intimacy: "),text().bind(tf).placeholder("Intimacy text"), select().bind(Buffer.from(IntimacyStrength.valuesS), option, is), Button(Glyphicon.PlusSign()).onClick(f => {
        println(s"clicked button and tf size == ${tf.get.length}")
        if (tf.get.length > 0 && is.get.isDefined) {
          println("Processing")
          val s = tf.get
          val l = is.get.get
          tf.:=("")
          is.:=(Some("Minor"))
          val c = IntimacyAddChange(s, l)
          if (char.canApplyChange(c))
          char.applyChange(c)
        }
      }))), 2))
    )
  }

  def willpowerView(implicit char: Character, charmdb: CharmDB, selectorModal: (Set[Seq[AtomicExchange]]) => Future[Option[Seq[AtomicExchange]]]): bindings.HTML.Container.Generic = {
    div(div(accordion("Willpower",
      new IntervalSelectorAtom(largestValue = Var(10), minimalValue = Var(5), currentValue = char.rxWillpower, generateChange = t => {
        val x = t - char.willpowerRating
        if (x > 0) {
          (1 to x).map(i => WillpowerIncreaseByOne(increase = true))
        } else if (x < 0) {
          (x to -1).map(i => WillpowerIncreaseByOne(increase = false))
        } else {
          Seq.empty
        }

      }, selectorModal = selectorModal), 2))
    )
  }

  def meritView(implicit char: Character, charmdb: CharmDB, selectorModal: (Set[Seq[AtomicExchange]]) => Future[Option[Seq[AtomicExchange]]]): bindings.HTML.Container.Generic = {

    val is :Var[Option[String]] = Var(Some(Merit.categories(0).toString))



    def comp(x : Seq[AtomicExchange]) = {
      val changeOpt: Future[Option[Seq[AtomicExchange]]] = selectorModal.apply(char.findChangePaths(x))
      println("Future is done")
      changeOpt.map(s => {
        println(s)
        s
      }).filter(_.isDefined).map(_.get).foreach(c => if (c.forall(p => char.canApplyChange(p))) {
        println("Writing from Selector")
        c.foreach(e => char.applyChange(e))
      } else {
        println("Not allowed anymore")
      })
    }


    def lbldBtn(lbl : String = "somelabel", source : Var[String] = Var("content"), doWhenClicked  : String => Option[AtomicExchange] = (s : String) => None, resetTimer : Int = 5000) = {
      val v = Var("")
      var typingTimer : Option[SetTimeoutHandle] = None

      def store() = {
        typingTimer.foreach(clearTimeout)
        typingTimer = None
        val o = doWhenClicked.apply(v.get)
        o.foreach( c => if (char.canApplyChange(c)) char.applyChange(c))
      }

      div(label(lbl), text().bind(v).placeholder("Name of Merit").subscribe(source).onKeyUp(f => {
        //reset after a while
        typingTimer.foreach(clearTimeout)
        typingTimer = Some(setTimeout(resetTimer) {
          if (v.get != source.get) {
            v.:=(source.get)
          }
        })
      }).onKeyPress(f => if (f.keyCode == KeyCode.Enter) store()), Button(Glyphicon.PlusSign()).onClick(f => store())).css("div-without-linebreak")
    }

    div(accordion("Merits", div(
      ul(char.rxMerits.map(merit => {
        li(lbldBtn(merit.category,  Var(merit.name), doWhenClicked = (s : String) => {
          Some(MeritNameChange(merit.id, merit.name, s))
        }), new IntervalSelectorAtom(currentValue = Var(merit.rating), generateChange = (i : Int) => {
          if (i > merit.rating) {
            (1 to i - merit.rating).map(s => MeritRatingChange(merit.id, 1))
          } else if (i < merit.rating) {
            (1 to merit.rating - i).map(s => MeritRatingChange(merit.id, -1))
          } else {
            Seq.empty
          }
        }, selectorModal = selectorModal), Button(Glyphicon.RemoveCircle()).onClick(f => {
          println(s"Pressed on $merit")
          val c = MeritRemoveChange(merit.id)
          if (char.canApplyChange(c)) {
            char.applyChange(c)
          }
        }))
      })),
      div(label("Add merit: "), select().bind(Merit.categories, option, is), Button(Glyphicon.PlusSign()).onClick(f => {

        if (is.get.isDefined) {
          println("Processing")
          val l = is.get.get
          is.:=(Some("Minor"))
          val c = MeritAddChange(l)
          if (char.canApplyChange(c)) {
            char.applyChange(c)
          }
        }
      }))
    )))
  }

  def Number(v : Channel[Int])(name : String, wi : Int = 50) = div(label(name), number().bind(v.biMap(i => i.toString, s => Integer.parseInt(s)))).width(Length.Pixel(wi))
  def Text(v : Channel[String])(name : String, wi : Int = 50) = div(label(name), text().placeholder(name).bind(v)).width(Length.Pixel(wi))
  def Check(v : Channel[Boolean])(name : String, wi : Int = 50) = div(label(name), checkbox().bind(v)).width(Length.Pixel(wi))

  def itemView(implicit char: Character, charmdb: CharmDB, selectorModal: (Set[Seq[AtomicExchange]]) => Future[Option[Seq[AtomicExchange]]]): bindings.HTML.Container.Generic = {
    val weapon = Var(false)
    val armor = Var(false)
    val soak = Var(0)
    val hardness = Var(0)
    val damage = Var(0)
    val accuracy = Var(0)
    val title = Var("")


    def liItem(i : AtomExal.CaseClasses.Items.Item) = {
      li(label(i.toString), Button(Glyphicon.Minus()).onClick(f => {
        val c = ItemRemoveChange(i.id)
        if (char.canApplyChange(c)) {
          char.applyChange(c)
        }
      }))
    }

    accordion("Items", div(
      div(h3("Weapons"), ol(char.rxItems.filter(_.isWeapon).map(i => liItem(i))))
      , div(h3("Armors"), ol(char.rxItems.filter(_.isArmor).map(i => liItem(i))))
      , div(h3("Misc."), ol(char.rxItems.filter(i => !i.isWeapon && !i.isArmor).map(i => liItem(i))))
      , div(h3("Add new item"),
        text().placeholder("title").bind(title) ,
        Check(weapon)("IsWeapon"),
        Check(armor)("IsArmor"),
        Number(soak)("Soak").visible(armor),
        Number(hardness)("Hardness").visible(armor),
        Number(damage)("Damage").visible(weapon),
        Number(accuracy)("Accuracy").visible(weapon),
        Button(Glyphicon.Plus()).onClick(f => {
          println("Adding item!!!")
          val c = ItemAddChange(title.get, weapon.get, armor.get, soak.get, damage.get, accuracy.get)
          title.:=("")
          if (char.canApplyChange(c)) {
            char.applyChange(c)
          }
        }).enabled(title.map(s => s != null && !s.isEmpty))
      )
    ), 2)
  }

  def charmableView(implicit char: Character, charmdb: CharmDB, ressourceManager: RessourceManager, selectorModal: (Set[Seq[AtomicExchange]]) => Future[Option[Seq[AtomicExchange]]]): bindings.HTML.Container.Generic = {
    import Helper.WidokVarHelpers._

    val directCharms = charmdb.states.filter(f => f == CharmDB.DirectReachable).keySeq
    val indirectCharms = charmdb.states.filter(f => f == CharmDB.ExtendedReachable).keySeq
    val restrictedCharms = charmdb.states.filter(f => f == CharmDB.Unreachable).keySeq



    def comp(x : Seq[AtomicExchange]) = {
      val changeOpt: Future[Option[Seq[AtomicExchange]]] = selectorModal.apply(char.findChangePaths(x))
      println("Future is done")
      changeOpt.map(s => {
        println(s)
        s
      }).filter(_.isDefined).map(_.get).foreach(c => if (c.forall(p => char.canApplyChange(p))) {
        println("Writing from Selector")
        c.foreach(e => char.applyChange(e))
      } else {
        println("Not allowed anymore")
      })
    }

    accordion("Charms (TODO)", div( //TODO: implement graphbased gui
      Button("My Charms").attribute("data-toggle", "collapse").attribute("data-target", "#havecharms"),
      ul(
        char.rxCharms
          .map(b => {
            val charm = charmdb(b)
            li("Possesses: "
              + charm.name
            )
          }
          )
      ).css("collapse").id("havecharms"),
      Button("Charms I can buy directly").attribute("data-toggle", "collapse").attribute("data-target", "#directcharms"),
      ul(
        directCharms
          .map(b => {
            val charm = charmdb(b)
            val enable = charm.requirement.eval()
            println(s"Charm $charm is enabled : $enable")
            val l = li("Hello "
              + charm.name,
              Button(Glyphicon.Plus).enabled(enable == CharmDB.DirectReachable).onClick(c => {
                comp(charmdb.pathToPurchase(b))
              })
            )
            if (enable == CharmDB.DirectReachable) l.css("charm-list-element") else l.css("charm-list-black")
          }
          )
      ).css("collapse").id("directcharms"),
      Button("Charms I can buy indirectly").attribute("data-toggle", "collapse").attribute("data-target", "#indirectcharms"),
      ul(
        indirectCharms
          .map(b => {
            val charm = charmdb(b)
            val enable = charm.requirement.eval()
            println(s"Charm $charm is enabled : $enable")
            val l = li("Hello "
              + charm.name,
              Button(Glyphicon.Plus).enabled(enable == CharmDB.DirectReachable).onClick(c => {
                comp(charmdb.pathToPurchase(b))
              })
            )
            if (enable == CharmDB.DirectReachable) l.css("charm-list-element") else l.css("charm-list-black")
          }
          )
      ).css("collapse").id("indirectcharms"),
      Button("Charms I cannot buy").attribute("data-toggle", "collapse").attribute("data-target", "#unpermittedcharms")
      ,
      ul(
        restrictedCharms
          .map(b => {
            val charm = charmdb(b)
            val enable = charm.requirement.eval()
            println(s"Charm $charm is enabled : $enable")
            val l = li("Hello "
              + charm.name,
              Button(Glyphicon.Plus).enabled(enable == CharmDB.DirectReachable).onClick(c => {
                comp(charmdb.pathToPurchase(b))
              })
            )
            if (enable == CharmDB.DirectReachable) l.css("charm-list-element") else l.css("charm-list-black")
          }
          )
      ).css("collapse").id("unpermittedcharms"), 4)
    )
  }
}
