package example.pages


import AtomExal.AtomicExchange
import AtomImpl.{Character, RessourceManager}
import org.widok._
import org.widok.{StateChannel, _}
import org.widok.bindings.Bootstrap.{Button, Checkbox, Glyphicon, Label, Modal, ModalBuilder}
import org.widok.html._
import CharSheetSubElements._
import Charms.CharmDB
import Model.CharmDatabase
import org.scalajs.dom.Blob
import org.widok.bindings.HTML._

import scala.collection.mutable.ListBuffer
import scala.concurrent.Future
import scala.concurrent.Future
import scala.scalajs.concurrent.JSExecutionContext.Implicits.queue
import scala.concurrent.Future
import scala.scalajs.js
import scala.concurrent.Future
import scala.concurrent.Future
import scala.scalajs.js.URIUtils
import scala.util.Try

/**
  * Created by Christopher on 17.07.2016.
  */
object CharacterView
  extends PageApplication {

  import example.helper.CharacterSubViews._


  val modal: ModalBuilder = ModalBuilder(
    Modal.Header(
      Modal.Close(modal.dismiss)
      , Modal.Title("Modal title")
    )
    , Modal.Body("Modal body")
    , Modal.Footer(
      Button("Submit").onClick(_ => modal.dismiss())
    )
  )

  implicit val selectorModal: (Set[Seq[AtomicExchange]]) => Future[Option[Seq[AtomicExchange]]] = (set: Set[Seq[AtomicExchange]]) => Future {
    if (set.size == 1) {
      //just give out only option
      Some(set.head)
    } else if (set.size < 1) {
      //nothing possible
      None
    } else {
      //show modal to select
      modal.open()
      ???
    }
  }
  implicit val character: Character = new Character()
  implicit val charmDB = CharmDB.default
  implicit val ressourceManager = new RessourceManager()
  ressourceManager.setDefaultCharacter()


  import upickle.default._


  val loadmodal: ModalBuilder = {
    val x = Var("Post JSON here")
    val y = x.biMap(s => Try {
      read[ListBuffer[AtomicExchange]](s)
    }.toOption, (list: Option[ListBuffer[AtomicExchange]]) => list.map(p => write(p)).getOrElse("[]")).cache
    val b = y.map(_.isDefined)
    ModalBuilder(
      Modal.Header(
        Modal.Close(loadmodal.dismiss)
        , Modal.Title("Import Character Progress from JSON")
      )
      , Modal.Body(Input.Text().bind(x))
      , Modal.Footer(
        Button("Load JSON").onClick(_ => {
          ressourceManager.setDefaultCharacter()
          y.get.foreach(l => l.foreach(c => if (character.canApplyChange(c)) {
            character.applyChange(c)
          } else {
            assert(false)
          }))
          loadmodal.dismiss()
        }).enabled(b)
      )
    )
  }


  object dynamicHelper {

    import scala.scalajs.js
    import js.DynamicImplicits._
    import js.Dynamic.{global => g}

    /**
      * this is very ugly, but a good workaround to use the filesaver.js without a scala.js facade
      *
      * @param content
      * @param filename
      */
    def storeJSON(content: String, filename: String): Unit = {
      //requires content to be json
      js.eval(
        s"""saveAs(new Blob([JSON.stringify($content)], {type: "text/plain;charset=utf-8"}), "$filename");""")
    }
  }


  val savemodal: ModalBuilder = {
    val x = Var("Click to view current character json")
    ModalBuilder(
      Modal.Header(
        Modal.Close(savemodal.dismiss)
        , Modal.Title("Export Character Progress to JSON")
      )
      , Modal.Body(x).onClick(f => {
        Future {
          write(character.improvisedLog)
        }.foreach(e => x.:=(e))
      })
      , Modal.Footer(
        Button("Download as file").onClick(f => {
          Future {
            write(character.improvisedLog)
          }.foreach(e => dynamicHelper.storeJSON(e, ("loc-" + System.currentTimeMillis() / 1000 % 1000000).toString + ".json"))
          savemodal.dismiss()
        })
        , Button("Close").onClick(_ => savemodal.dismiss())
      )
    )
  }


  def view() = Inline(
    div(modal, loadmodal, savemodal
      , h1("This is Loom Of Characters, enjoy"))
    , Button("Open Modal").onClick(_ => modal.open())
    , Button("Load Character").onClick(_ => loadmodal.open())
    , Button("Save Character").onClick(_ => savemodal.open())
    , controlBar
    , hr()
    , experiencableView
    , characterableView
    , attributableView
    , abilitableView
    , charmableView
    , willpowerView
    , intimacyView
    , meritView
    , itemView
  )

  def ready() {}


}
