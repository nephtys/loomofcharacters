package example.pages


import Controls.{AbilityMultiControl, IntervalPoints}
import Manager.RessourceManager
import Viewmodel.{Attribute, Specialty}
import org.widok.{StateChannel, _}
import org.widok.bindings.Bootstrap.{Button, Glyphicon, Label}
import org.widok.html._
import CharSheetSubElements._
import Model.{CharmDatabase, ModelCharacter}

/**
  * Created by chris on 08.07.2016.
  */
case class CharacterSheetPage() extends Page {

  val ressourcemanager : RessourceManager = new RessourceManager()

  def vm = ressourcemanager.currentCharacter.get.get //TODO: change this to allow scenes without loaded character

  implicit val implvm : StateChannel[Viewmodel.Character] = Var(vm)
  implicit val implcharmdb = Var(new CharmDatabase {})
  implicit val implchar = Var(new ModelCharacter {})


  def view(): View =  div(
    h1("Ability Test Area:")
      ,new AbilityMultiControl(group = vm._abilityGroups)
      ,

    h1("hello to my character")
    ,ul(
      Buffer(
        li(
          field("Name", vm._name)
        ).css("flex-item-meta")
        ,li(
          field("Player", vm._player)
        ).css("flex-item-meta")
        ,li(
          field("Caste" ,select().default("").bind(vm._castesPossibleStr, option, vm._casteStr))
        ).css("flex-item-meta")
        ,li(
          field("Concept", vm._concept)
        ).css("flex-item-meta")
        ,li(
          field("Anima", vm._anima)
        ).css("flex-item-meta")
      )
    ).css("flex-container-meta")
    ,hr()
    //,raw("""<ul class="flex-container">  <li class="flex-item">1</li>  <li class="flex-item">2</li>  <li class="flex-item">3</li>  <li class="flex-item">4</li>  <li class="flex-item">5</li>  <li class="flex-item">6</li></ul>""")
    ,attributes(vm._attributes)
    ,hr()
    ,specialties(vm._specialties, vm._abilityFamilies)
    ,hr()
    ,merits(vm._merits, vm._selectableMeritCategory)
  )




  def ready(route: InstantiatedRoute) {
    log(s"Page 'main' loaded with route '$route'")
  }

  override def destroy() {
    log("Page 'main' left")
  }
}
