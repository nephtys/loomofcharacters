package example.pages

import Controls.IntervalPoints
import Viewmodel.Specialty
import org.widok.bindings.Bootstrap.Glyphicon
import org.widok.html._
import org.widok.{Buffer, Channel, Var, View}

/**
  * Created by chris on 09.07.2016.
  */
object CharSheetSubElements {
  def field(Description : String, channel : Channel[String]) : View = {
    div(label(Description).attribute("style", "margin-right: 15px;"), text().bind(channel).placeholder("Name of Specialty"))
  }
  def field(Description : String, channel : Channel[String], placeholder : String) : View = {
    div(label(Description).attribute("style", "margin-right: 15px;"), text().bind(channel).placeholder(placeholder) )
  }
  def fieldH(Description : String, channel : Channel[String], placeholder : String) : View = {
    div(label(Description).attribute("style", "margin-right: 15px;"), text().bind(channel).placeholder(placeholder) ).attribute("style", "display: inline-block;")
  }
  def field(Description : String, view : View ) : View = {
    div(label(Description).attribute("style", "margin-right: 15px;"), view)
  }
  def fieldH(Description : String, view : View ) : View = {
    div(label(Description).attribute("style", "margin-right: 15px;"), view).attribute("style", "display: inline-block;")
  }

  def specialties( specialtiesSelected : Buffer[Specialty], possibleAbilityFamilies : Buffer[String]) : View = {
    val specialtyToAdd = Var("")
    val selectedFamily : Var[Option[String]] = Var(Some(""))
    val defaultS = ""
    div(
      h3("Specialties")
      , ul(
        specialtiesSelected.map( (ar : Specialty) => li(
          fieldH(ar.abilityFamily, fieldH(ar.title, a(Glyphicon.Remove()).onClick(e => specialtiesSelected.-=(ar))))
        )
        )
      ) //delete old specialties
      ,div(select().default(defaultS).bind(possibleAbilityFamilies, option, selectedFamily).attribute("style", "display: inline-block;"), fieldH("Title", specialtyToAdd, "Name of Specialty"), button(Glyphicon.PlusSign()).onClick(e => selectedFamily.get.filter(f => f != null && f.length > 0).foreach(f => if(specialtyToAdd.get.length > 0) specialtiesSelected.+=(new Specialty(f, specialtyToAdd.get))))) //here add new ones with text and selection
    )
  }

  def attributes( viewm : Seq[Viewmodel.Attribute]) : View = {
    ul(
      Buffer.from(viewm.map( (ar : Viewmodel.Attribute) => li(
        field(ar.name, new IntervalPoints().bind(ar.rating))
      )
      ))
    )
  }

  def merits(mers : Buffer[Viewmodel.Merit], possibleMeritCategories : Buffer[String]) : View = {
    //val meritToAdd = Var("")
    val selectedCategory : Var[Option[String]] = Var(Some(""))
    val defaultS = ""
    div(
      h3("Merits")
      , ul(
        mers.map( (ar : Viewmodel.Merit) => li(
          div(fieldH(ar.category, a(Glyphicon.Remove()).onClick(e => mers.-=(ar))), new IntervalPoints() {currentValue.bind(ar.rating)})
        )
        )
      )
      ,div(fieldH("Category", select().default(defaultS).bind(possibleMeritCategories, option, selectedCategory).attribute("style", "display: inline-block;")), button(Glyphicon.PlusSign()).onClick(e => {
        println("Pressed Add Merit")
        selectedCategory.get.filter(_.length > 0).foreach(s => mers.+=(new Viewmodel.Merit(s)))
      } ))
    )
  }
}
