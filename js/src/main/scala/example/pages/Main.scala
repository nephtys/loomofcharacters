package example.pages

import Controls.{CloseableModal, IntervalPoints}
import org.widok._
import org.widok.html._
import org.widok.bindings.HTML
import example._
import org.widok.bindings.Bootstrap._

case class Main() extends Page {
  val name = Var("")
  val hasName = name.map(_.nonEmpty)

  val numbers = Buffer("one", "two", "three")
  val selection = Var(Option.empty[String])


  val a = Var(1)
  val b = Var(2)
  println(a.get)
  println(b.get)
  b.bind(a)
  println(a.get)
  println(b.get)

  //val modal = new CloseableModal(Label("Hello World!"))


  def view() = div(
    h1("Welcome to Widok!")
    , Button(Glyphicon.Bitcoin())

    , new IntervalPoints() {
      currentValue.bind(a)
    }

    //, modal.getButtonToOpen
    //, modal

    , p("Please enter your name:")

    , text().bind(name)

    , p("Hello, ", name)
      .show(hasName)

    , Button("Change my name to TuX")
      .onClick(_ => name := "TuX")
      .show(name.isNot("TuX")),

    button("Visit personalized Page")
      .onClick(_ => {
        val route: InstantiatedRoute = Routes.test("param", name.get)
        route.go()
      })
      .show(hasName)

    , button("Log out")
      .onClick(_ => name := "")
      .show(hasName)
    , hr()
    , HTML.Anchor("Link to second page")
      .url(Routes.test("param", "first page"))
    , hr()

    , select().default("").bind(numbers, option, selection)
      ,p("Selected: ", selection.map(_.getOrElse("N/A"))).show(selection.map(_.nonEmpty))
  )


  a.:=(3)

  def ready(route: InstantiatedRoute) {
    log(s"Page 'main' loaded with route '$route'")
    Character.Converter.load() //test call to Shared Code
  }

  override def destroy() {
    log("Page 'main' left")
  }
}
