package Manager

import Viewmodel.Specialty
import org.widok.Var

/**
  * Created by Christopher on 05.07.2016.
  */
class RessourceManager {

  val currentCharacter : Var[Option[Viewmodel.Character]] = Var(None)

  currentCharacter := Some(new Viewmodel.Character() {
    this._attributesMap.get("Dexterity").foreach(a => a.rating.:=(5))
    this._specialties.+=(new Specialty(_abilityFamilies.get.head, "Cool Stuff"))
    this._specialties.+=(new Specialty(_abilityFamilies.get(2), "Big Swords"))
  })

  def load = ???
  def store = ???
  def create = ???
}
