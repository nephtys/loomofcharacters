package Helper

import org.widok.Channel

import scala.concurrent.Future
import scala.scalajs.js.timers._

/**
  * Created by nephtys on 8/28/16.
  */
object PlatformDependentHelper extends PlatformDependantHelperTrait{

  def debounce[T](x: Channel[T])(ms: Long): Channel[T] = {
    val p = Channel[T]()
    var nextvalue : Option[T] = None
    var handle : Option[SetTimeoutHandle] = None
    x.attach(t => {
      nextvalue = Some(t)
      if(handle.isEmpty) {
        handle = Some(setTimeout(ms){
          nextvalue.foreach(e => p.:=(e))
          nextvalue = None
          handle = None
        })
      }
    })
    p
  }
}
