package Helper


import org.widok.{BufSet, Buffer, Channel, DeltaDict, Dict, ReadBufSet, ReadBuffer}

import autowire._
import scala.concurrent.ExecutionContext.Implicits.global
import upickle._
import scala.concurrent.Future
import scala.scalajs.js.timers._

/**
  * Created by nephtys on 8/28/16.
  */
object WidokSJSVarHelpers {



  implicit class SomeChannel[T](x : Channel[T]) {
    def debounce(ms : Long) : Channel[T] = {
      val p = Channel[T]()
      var nextvalue : Option[T] = None
      var handle : Option[SetTimeoutHandle] = None
      x.attach(t => {
        nextvalue = Some(t)
        if(handle.isEmpty) {
          handle = Some(setTimeout(ms){
            nextvalue.foreach(e => p.:=(e))
            nextvalue = None
            handle = None
          })
        }
      })
      p
    }
    def foreach(i : T => Unit) : Unit = x.attach(i)
    def backgroundforeach(i : T => Unit) : Unit = x.attach(v => Future{i.apply(v)})
  }

}
