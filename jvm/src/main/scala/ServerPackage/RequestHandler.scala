package ServerPackage

import ServerHelper.ServableStaticFiles
import colossus.core._
import colossus.protocols.http.HttpMethod._
import colossus.protocols.http.UrlParsing._
import colossus.protocols.http._
import colossus.service.Callback

/**
  * Created by chris on 07.07.2016.
  */
class RequestHandler(context: ServerContext) extends HttpService(context)  {


  val htmlFiles = new ServableStaticFiles {
    override def dir: String = "web"

    override def typ: String = "html"
  }
  val jsFiles = new ServableStaticFiles {
    override def dir: String = "web/js"

    override def typ: String = "js"
  }



  def handle = {
    case request @ Get on Root / "hello" => {
      Callback.successful(request.ok("Hello World!"))
    }
    case request @ Get on Root / "echo" / str => {
      Callback.successful(request.ok(s"Received: $str"))
    }
      //static files:
    case request @ Get on Root / "web" / "js" / filename => {
      Callback.successful(jsFiles.get(filename).map(r => request.ok(r)).getOrElse(request.notFound("N/A")))
    }
    case request @ Get on Root / "web" / filename => {
      Callback.successful(htmlFiles.get(filename).map(r => request.ok(r).withHeader("Content-Type", "text/html")).getOrElse(request.notFound("N/A")))
    }
  }
}
