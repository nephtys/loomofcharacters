package ServerPackage

import AtomImpl.{Character, RessourceManager}
import _root_.Character.Converter
import Charms.CharmDB
import Model.CharmDatabase
import ServerHelper.ServerInitializer
import akka.actor.ActorSystem
import colossus.IOSystem
import colossus.core.Server
import org.widok.Var
import Helper.WidokVarHelpers._

/**
  * Created by chris on 07.07.2016.
  */
object LoomServer extends App {

  implicit val character: Character = new Character()
  implicit val charmDB = CharmDB.default
  implicit val ressourceManager = new RessourceManager()
  ressourceManager.setDefaultCharacter()

  val x = Var(0)

  Helper.PlatformDependentHelper.debounce(x)(500).foreach(i => println(s"debounce: $i"))

  x.foreach(i => println(s"x: $i"))


  println("Working!")


  Converter.load()
  println("JVM only Code")


  implicit val system = ActorSystem.create("LoomOfFateActors")

  implicit val io = IOSystem()

  Server.start("hello-world", 9000){ worker => new ServerInitializer(worker) }


  (1 to 3).foreach( i => {
    Thread.sleep(100)
    x.:=(i)
  } )
}
