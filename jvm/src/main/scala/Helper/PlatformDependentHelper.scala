package Helper

import org.widok.Channel

import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global

import scala.concurrent.Future

import scala.concurrent.Future

/**
  * Created by nephtys on 8/28/16.
  */
object PlatformDependentHelper extends PlatformDependantHelperTrait{

  def debounce[T](x: Channel[T])(ms: Long): Channel[T] = {
    val p = Channel[T]()
    var nextvalue : Option[T] = None
    var future : Option[Future[Unit]] = None
    x.attach(t => {
      nextvalue = Some(t)
      if(future.isEmpty) {
        future = Some(Future{
          Thread.sleep(ms)
          nextvalue.foreach(e => p.:=(e))
          nextvalue = None
          future = None
        })
      }
    })
    p
  }
}
