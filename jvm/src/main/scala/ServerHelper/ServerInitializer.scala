package ServerHelper

import ServerPackage.RequestHandler
import colossus.core.{Initializer, WorkerRef}

/**
  * Created by chris on 07.07.2016.
  */
class ServerInitializer (worker: WorkerRef) extends Initializer(worker) {
  //can probably be compacted into anonymous class
  def onConnect = context => new RequestHandler(context)

}
