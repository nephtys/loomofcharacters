package ServerHelper

import java.io.File
import java.nio.file.{Path, Paths}

import scala.io.Source

/**
  * Created by chris on 07.07.2016.
  */
trait ServableStaticFiles {
  //does not work recursively (wouldn't be needed)

  def typ : String
  def dir : String

  private def files : Seq[File] = getListOfFiles(dir)
  private def filter(filename : String)   = filename.filterNot(c => c == '/' || c == '\\').mkString //to basic sanitize on UNIX
  def existsInDir(filename : String) : Boolean = files.exists(f => f.getName == filter(filename))
  def toPath(filename : String) : String = dir + File.separator + filename
  def get(filename : String) : Option[String] = if (existsInDir(filename)) {
    Some(filecontents(toPath(filter(filename))))
  } else None

  private def filecontents(s : String) : String = Source.fromFile(s, "UTF8").getLines.mkString


  private def getListOfFiles(dir: String):Seq[File] = {
    val d = new File(dir)
    if (d.exists && d.isDirectory) {
      d.listFiles.filter(_.isFile).toSeq
    } else {
      Seq[File]()
    }
  }
}
