import java.nio.file.Files

import sbt._
import sbt.Keys._
import org.scalajs.sbtplugin._
import org.scalajs.sbtplugin.ScalaJSPlugin.autoImport._
import org.scalajs.sbtplugin.cross.CrossProject

object Build extends sbt.Build {

  val outPath = new File("web")
  val jsPath = outPath / "js"
  val cssPath = outPath / "css"
  val fontsPath = outPath / "fonts"

  val buildOrganisation = "example"
  val buildVersion = "0.1-SNAPSHOT"
  val buildScalaVersion = "2.11.8"
  val buildScalaOptions = Seq(
    "-unchecked", "-deprecation"
    , "-encoding", "utf8"
    , "-Xelide-below", annotation.elidable.ALL.toString
  )


  lazy val crossProject =
    CrossProject(
      "server", "client", file("."), CrossType.Full
    )
      .settings(
        /* Shared settings */
        organization := buildOrganisation
        , version := buildVersion
        , scalaVersion := buildScalaVersion
        , scalacOptions := buildScalaOptions
        , persistLauncher := true
        ,libraryDependencies += "com.lihaoyi" %%% "scalarx" % "0.3.1"
        ,libraryDependencies += "com.lihaoyi" %%% "upickle" % "0.4.1"
        ,libraryDependencies += "com.softwaremill.macwire" %% "macros" % "2.2.3" % "provided"
        ,libraryDependencies += "com.softwaremill.macwire" %% "util" % "2.2.3"
        ,libraryDependencies += "com.softwaremill.macwire" %% "proxy" % "2.2.3"
      )
      .jsSettings(
        /* Scala.js settings */
        libraryDependencies ++= Seq(
          "io.github.widok" %%% "widok" % "0.2.2"
          ,"com.lihaoyi" %%% "autowire" % "0.2.5"
        )
        ,artifactPath in (Compile, packageScalaJSLauncher) :=
          jsPath / "launcher.js"
        , artifactPath in (Compile, fastOptJS) :=
          jsPath / "application.js"
      )
      .jvmSettings(
        /* JVM settings */
        libraryDependencies += "com.typesafe.akka" % "akka-actor_2.11" % "2.3.9",
        libraryDependencies += "com.tumblr" %% "colossus" % "0.8.0",
        libraryDependencies += "io.github.widok" %% "widok" % "0.2.2",
        libraryDependencies += "com.lihaoyi" %% "upickle" % "0.4.1",
        libraryDependencies += "com.lihaoyi" %% "autowire" % "0.2.5",
        sourceManaged := file("jvm/target"),
        mainClass   := Some("ServerPackage.LoomServer")
      )

  lazy val js = crossProject.js
  lazy val jvm = crossProject.jvm

  /*lazy val main = Project(id = "example", base = file("js"))
    .enablePlugins(ScalaJSPlugin)
    .settings(
    )*/

}